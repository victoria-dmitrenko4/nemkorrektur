<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nemkorrektur_db');

/** MySQL database username */
define('DB_USER', 'newuser');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BFmw4sxuiAZq8(0V#K2u#V)O*P&Pho1ES%Ua32PXNWTonD)UEcSo0RCHJ2&5gT38');
define('SECURE_AUTH_KEY',  'WOEEcuRv6NWxcc2NlCYSiJkpOocgWhxJikp0iUH0CPrmblskeZ3G(p0@wU2^IO94');
define('LOGGED_IN_KEY',    '0D&&33(ZfGdURvfNsnsjhvUby4VZucWFPph!GbF#)QA3U0PZYg9M^FzTBh8D*P(L');
define('NONCE_KEY',        'sL8MVH#uRCpoQQBpItw^Zllc^nuLu&anQXGI&5aCO51Rsseafhll^GX7%i%JVF1#');
define('AUTH_SALT',        'UVPq%P##dJs#ulT4L^WjPr5BUQ6H&2V!zrEVZ4Uadax6kK4ChMlgXDjXXRKBHH#n');
define('SECURE_AUTH_SALT', 'mV8ORXIvdTcq2F*0g%3&yZX*3ot@3B0J@MwomZJNkWMMusF%Ggfccn*OVd%@rVr1');
define('LOGGED_IN_SALT',   'xhE7YhLt@Y8U#w%OXyWibJirEZm6Y%T9HQShhEY*DaC(%h0X9uEhyJ31@b2@Os&z');
define('NONCE_SALT',       ')hkbOLEkY*!3#9a^ASOmo2tRcev(uOW7nefVa%0MUcefGvROWwsilzSPg*Q4pcK%');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wordpwp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');
