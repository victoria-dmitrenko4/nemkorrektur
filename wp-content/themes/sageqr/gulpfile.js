//gulpfile.js
'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    prefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    cssmin = require('gulp-minify-css'),
    cleanCSS = require('gulp-clean-css'),
    // watch = require('gulp-watch'),
    browserSync = require("browser-sync"),
    plumber = require("gulp-plumber"),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    criticalCss = require('gulp-penthouse'),
    reload = browserSync.reload;

var path = {
    build: {
        css: 'dist/styles',
    },
    src: {
        style: 'resources/assets/styles/style.scss',
        js: 'resources/assets/scripts/myajax.js',
    },
    watch: {
        style: 'resources/assets/styles/**/style.scss',
        js: 'resources/assets/scripts/**/myajax.js',
    }
};

function css() {
    return gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(cleanCSS())
        .pipe(cssmin())
        .pipe(concat('style.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream());
}

function js() {
    return gulp.src(path.src.js)
        .pipe(browserSync.stream());
}


function watcher() {
    browserSync.init({
        proxy: "http://nemkorrektur.loc",
        open: true,
        notify: false
    });
    gulp.watch([path.watch.style], css).on('change', browserSync.reload);
    gulp.watch([path.watch.js], js).on('change', browserSync.reload);

}

const watch = gulp.parallel(watcher);
exports.watch = watch;
