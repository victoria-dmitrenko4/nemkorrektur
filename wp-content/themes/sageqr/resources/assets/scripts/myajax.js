var $ = jQuery;
$(document).ready(function () {

    jsonObj = [];

    // share with
    $('#share_btn').click(function (e) {
        e.preventDefault();
        $('#share_modal').modal('show');
    });

    // Remove File
    $(document).on('click', '#file_remove', function () {

        var active_img = $('.figure.active .current'),
            file_src = active_img.attr('data-src'),
            curr_path = $('[name="usr_upload_dir"]').val(),
            folder_name = $.trim($('.clicked .folder_name').text());

        if (file_src === undefined) {
            var type = 'folder';
            file_src = folder_name;
        }

        var data = {
            action: 'myajax-remove',
            nonce_code: the_ajax_script.nonce,
            path: curr_path,
            fileName: file_src,
        };

        alert('This file will be removed! Are you sure?');

        $.post(the_ajax_script.myajaxurl, data, function (response) {

            if (response) {

                if (type === 'folder') {
                    $('.folder-bar.clicked').remove();
                } else {
                    $('figure.active').parents().eq(1).remove();
                }

            } else alert('ERROR: You cannot remove this file');

        });

    });

    // Rename File
    $(document).on('click', '#file_rename', function () {
        $('#renFileModal').modal('show');

        var active_img = $('.figure.active .current'),
            file_src = active_img.attr('data-src'),
            curr_user = active_img.attr('data-usr'),
            curr_path = $('[name="usr_upload_dir"]').val(),
            folder_name = $.trim($('.clicked .folder_name').text());


        if (file_src != undefined) {

            $('[name="renfile"]').val(file_src);

            var type = 'file';

        } else {
            $('[name="renfile"]').val(folder_name);

            var type = 'folder';
            file_src = folder_name;
        }

        $(document).on('change', '[name="renfile"]', function () {

            var newFileName_src = $('[name="renfile"]').val();

            $("#renFileModal").submit(function (event) {

                event.preventDefault();

                var data = {
                    action: 'rename_item',
                    nonce_code: the_ajax_script.nonce,
                    path: curr_path,
                    fileName: file_src,
                    newFileName: newFileName_src,
                    currentUser: curr_user
                };
                $.post(the_ajax_script.myajaxurl, data, function (response) {

                    if (response) {

                        if (type === 'file') {

                            $('figure.active .figure-caption .list-inline-item span:last-child').text(newFileName_src);

                        } else {

                            $('.clicked .folder_name').text(newFileName_src).attr('customid', $.trim(newFileName_src));
                            $('.clicked .folder_info_wrapper').attr('customid', $.trim(newFileName_src));

                        }

                        $('#renFileModal').modal('hide');

                    } else {
                        alert('ERROR: You cannot rename this file');
                    }

                });

            });
        });


        return true;

    });

    // view folder
    $(document).on('dblclick', '.folder-double-click', open_folder_onDBClick);

    //folder actions
    $(document).on('click', '.folder-double-click', function () {

        $('#favourite_btn, #view_btn, #move_btn, #expiry_btn, #updaterev-form label').addClass('disabled');

        $('.folder-double-click').removeClass('clicked');
        $('#card_figure .figure').removeClass('active');
        $(this).addClass('clicked');

        fileActions();
    });

    // image actions
    $(document).on('click', '.fig_img', function () {

        $('#favourite_btn, #view_btn, #move_btn, #expiry_btn, #updaterev-form label').removeClass('disabled');

        $('.folder-double-click').removeClass('clicked');

        fileActions();
    });

    //breadcrumbs
    $(document).on('click', '.breadCrumb', function () {

        var path = $(this).attr('data-path');
        var step = $(this).attr('data-step');

        step++;

        if (step <= 5) {

            for (var i = step; i <= 4; i++) {
                var name = 'step-' + i;
                $('#document_info').removeAttr(name);

            }
            $('#document_info').attr('folder_level', step - 1);

            var step1 = $('#document_info').attr('step-1'),
                step2 = $('#document_info').attr('step-2'),
                step3 = $('#document_info').attr('step-3'),
                step4 = $('#document_info').attr('step-4');


        } else {

            var step1 = $('#document_info').attr('step-1'),
                step2 = $('#document_info').attr('step-2'),
                step3 = $('#document_info').attr('step-3'),
                step4 = $('#document_info').attr('step-4');

        }

        $.ajax({
            type: 'POST',
            async: false,
            url: the_ajax_script.myajaxurl,
            data: {
                action: 'render_folder',
                folder_path: path,
                step1: step1,
                step2: step2,
                step3: step3,
                step4: step4,
            },
            success: function (data) {

                $('#contentdata').html(data);

                $('#folderBreadCrumb li:nth-last-child(2)').hide();
                $('input[name="usr_upload_dir"]').val($('.breadCrumb.lastElem').attr('data-path'));

            }
        });
    });

    // load files
    $(document).on('change', '#file ', function () {

        var files = this.files;
        var formData = new FormData();
        var upload_dir = $('input[name="usr_upload_dir"]').val();

        if (typeof files == 'undefined') return;
        formData.append('file', files[0]);
        formData.append('action', 'process_upload');
        formData.append('usr_upload_dir', upload_dir);
        $.ajax({
            url: the_ajax_script.myajaxurl,
            async: false,
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {

                console.log(data);
                debugger;

                $('#current_dir').html(data);

                $('.fig_img img').each(function () {

                    var img_src = $(this).attr('data-src');

                    upload_dir = upload_dir.substring(upload_dir.indexOf('wp-content'));

                    $(this).attr('src', upload_dir + '/' + img_src);

                });

                files.length = 0;

            },

            error: function (jqXHR, status, errorThrown) {
                console.log('ОШИБКА AJAX запроса: ' + status, jqXHR);
            }

        });

    });

    // create folder
    $(document).on('click', '#create_folder', function () {

        var upload_dir = $('input[name="usr_upload_dir"]').val(),
            folder_name = $('#folder_name').val(),
            folder_level = $('#document_info').attr('folder_level');

        $.ajax({
            type: 'POST',
            async: false,
            url: the_ajax_script.myajaxurl,
            data: {
                action: 'create_new_folder',
                usr_upload_dir: upload_dir,
                folder_name: folder_name
            },
            success: function (data) {

                $('#current_dir').html(data);

                $('.fig_img img').each(function () {

                    var img_src = $(this).attr('data-src');

                    upload_dir = upload_dir.substring(upload_dir.indexOf('wp-content'));

                    $(this).attr('src', upload_dir + '/' + img_src);

                });


                $('#exampleModal').modal('hide');

            },

            error: function (jqXHR, status, errorThrown) {
                console.log('ОШИБКА AJAX запроса: ' + status, jqXHR);
            }

        });

    });

    // close actions
    $(document).mouseup(function (e) {
        var container = $(".fig_img, .folder-double-click, #page_title_bar, input[name=\"renfile\"], #rename_btn");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('#fileaction').hide();
            $('.folder-double-click').removeClass('clicked');
            $('#card_figure .figure').removeClass('active');
        }
    });

    // share with
    $(document).on('keypress', '#share_modal .bootstrap-tagsinput input[type="text"]', function (e) {
        if (e.which == 13) {
            $('#share_submit, #cancel_share').addClass('visible');
        }

    });

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validate() {
        var $result = $("#invalid_email-error"),
            emails = new Array();

        $('#share_form .tag').each(function () {

            var email = $(this).text();
            if (validateEmail(email)) {
                $result.hide();
                emails.push(email);
            } else {
                $result.show();
            }

        });

        return emails;

    }

    $(document).on('click', '#share_submit', function (e) {
        e.preventDefault();

        var emails = validate();

        $('#shared_user > span').remove();

        console.log(emails);

        $.ajax({
            type: 'POST',
            async: false,
            url: the_ajax_script.myajaxurl,
            data: {
                action: 'share_item',
                emails: emails,
            },
            success: function (data) {

                debugger;

            },

            error: function (jqXHR, status, errorThrown) {
                console.log('ОШИБКА AJAX запроса: ' + status, jqXHR);
            }

        });

    });

    $('#cancel_share').on('click', function (event) {

        $('#share_submit').removeClass('visible');
        $('#cancel_share').removeClass('visible');
        $('#share_done').show();

    });
//     $('.shareAction').live('change', function () {
//         $('#share_done').hide();
//         $('#shareUser').hide();
//
//
//         var share_id = $(this).attr('data-share_id');
//         var share_to = $(this).attr('share_to');
//         var sharing_forward = $(this).attr('data-sharing_forward');
// //    console.log(sharing_forward);
//         var download = $(this).attr('data-download');
//         var share_action = $(this).val();
//         $('#sharing_forward').val(sharing_forward);
//         $('#download').val(download);
//         if (sharing_forward == 1) {
//             $('#uniform-sharing_forward>span').addClass("checked");
//         }
//         if (download == 1) {
//             $('#uniform-download>span').addClass("checked");
//         }
//         $('#save_changes').attr('data-share_id', share_id);
//         $('#save_changes').attr('share_to', share_to);
//         $('#save_changes').attr('data-share_action', share_action);
//         $('#save_changes').show();
//         $('#cancelBtn').show();
//         //$('#save_changes').attr('data-sharing_forward',share_id);
//         //$('#save_changes').attr('data-download',share_action);
//     });
//
//     $('#save_changes').live('click', function () {
//         var share_id = $(this).attr('data-share_id');
//         var share_action = $(this).attr('data-share_action');
//         var sharing_forward = $('#sharing_forward').val();
//         var download = $('#download').val();
//         if($('.add_background').length>0){
//             var share_to = $(this).attr('share_to');
//             saveChanges_forFolder(sharing_forward,share_id,share_action,download,share_to);
//             return true;
//         }
//         var version_id = $('#share_btn').attr('data-version_id');
//
//         var doc_data = [];
//         if ($('#grid_btn>i').hasClass('icon-grid')) {
//             if ($('.add_border_onlist').length > 1) {
//                 $('.add_border_onlist').each(function () {
//                     var document_id = $(this).attr("id").split("_")[1];
//                     var doc_object = {document_id: document_id}
//                     doc_data.push(doc_object);
//                 });
//                 saveChanges_forMulti(doc_data, share_id, share_action, sharing_forward, download);
//                 return true;
//             }
//         } else {
//             if ($('.add_border').length > 1) {
//                 $('.add_border').each(function () {
//                     var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
//                     var doc_object = {document_id: document_id}
//                     doc_data.push(doc_object);
//                 });
//                 saveChanges_forMulti(doc_data, share_id, share_action, sharing_forward, download);
//                 return true;
//             }
//         }
//
//         $('.slimScrollDiv').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
//
//
//         var dataa = {version_id: version_id, share_id: share_id, share_action: share_action, sharing_forward: sharing_forward, download: download};
//         $.post(base_url + "/document/index/updateShare", dataa, function (data) {
//
//         }).success(function () {
//             $('.page-loading').remove();
//             $('#share_done').show();
//             $('#shareUser').show();
//             $('#save_changes').hide();
//             $('#cancelBtn').hide();
//         });
//     });
//
//     $('.shareDelete').live('click', function () {
//         var share_id = $(this).attr('data-share_id');
//         var share_to = $(this).attr('share_to');
//         bootbox.confirm("Are you sure you wish to delete share.", function (result) {
//             if (result) {
//                 if($('.add_background').length>0){
//                     deleteShare_forFolder(share_id,share_to);
//                     return true;
//                 }
//                 var doc_data = [];
//                 if ($('#grid_btn>i').hasClass('icon-grid')) {
//                     if ($('.add_border_onlist').length > 1) {
//                         $('.add_border_onlist').each(function () {
//                             var document_id = $(this).attr("id").split("_")[1];
//                             var version_id = $(this).attr("data-version_id");
//                             var doc_object = {document_id: document_id, version_id: version_id}
//                             doc_data.push(doc_object);
//                         });
//                         deleteShare_forMulti(doc_data, share_id);
//                         return true;
//                     }
//                 } else {
//                     if ($('.add_border').length > 1) {
//                         $('.add_border').each(function () {
//                             var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
//                             var version_id = $(this).parent().eq(0).attr("data-version_id");
//                             var doc_object = {document_id: document_id, version_id: version_id}
//                             doc_data.push(doc_object);
//                         });
//                         deleteShare_forMulti(doc_data, share_id);
//                         return true;
//                     }
//                 }
//                 $('#shareUser').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
//                 var version_id = $('#share_btn').attr('data-version_id');
//                 var document_id = $('#share_btn').attr('data-document_id');
//                 var dataa = {version_id: version_id, share_id: share_id};
//                 $.post(base_url + "/document/index/deleteShare", dataa, function (data) {
//
//                 }).success(function () {
//                     $('#share_user_' + share_id).remove();
//                     $('.page-loading').remove();
//                     getactivity(document_id, 'D');
//                 });
//
//             }
//         });
//
//     });

});

function open_folder_onDBClick(e) {
    var elem = $(e.currentTarget),
        parent_name = $('#document_info').attr('folder_parent'),
        parent_id = parent_name.replace(/\s/g, '');

    $('#fileaction').hide();
    folderOpen(e, elem, parent_name, parent_id);

}

function folderOpen(e, elem, parent_name, parent_id, path) {

    var myObj = e.target,
        folder = elem.find('.folder_info_wrapper'),
        folder_name = $.trim(elem.find('.folder_name').text()),
        folder_id = folder.attr("customid"),
        path = folder.attr('data-path');


    var folder_level = $('#document_info').attr('folder_level');
    var current_folder_level = parseInt(folder_level);

    var title = $("#page_title_bar").find('h4').text();

    $('#document_info').attr('current_folder_name', $.trim(elem.text()));
    $('#document_info').attr('current_folder_name_tmp', $.trim(elem.text()));

    if ($('#document_info').attr("double_click_intrash") == 0) {
        $('#document_info').attr("double_click_intrash", 1);
        if ($.trim(title) == "Trash") {
            trashFolderRestoreBox(folder_id);
        } else {
            openFolder(folder_id, current_folder_level, folder_name, parent_name, parent_id, path);
            $('#document_info').attr("double_click_intrash", 0)
        }
    }
}

function openFolder(folder_id, current_folder_level, folder_name, parent_name, parent_id, path) {

    if ($('#document_info').attr("open_folder_ajax") == "0") {
        $('#document_info').attr("open_folder_ajax", "1");
    }

    var step1 = $('#document_info').attr('step-1'),
        step2 = $('#document_info').attr('step-2'),
        step3 = $('#document_info').attr('step-3'),
        step4 = $('#document_info').attr('step-4');

    $.ajax({
        type: 'POST',
        async: false,
        url: the_ajax_script.myajaxurl,
        data: {
            action: 'render_folder',
            folder_id: folder_id,
            parent_id: parent_name,
            folder_path: path,
            step1: step1,
            step2: step2,
            step3: step3,
            step4: step4,
        },
        success: function (data) {

            parentFolderInfoJSON(folder_id, folder_name, parent_name, jsonObj);

            current_folder_level++;

            $('#document_info').attr({
                'folder_parent': folder_id,
                'current_folder_id': folder_id,
                'folder_level': current_folder_level,
            });

            var step = 'step-' + current_folder_level;

            $('#document_info').attr(step, folder_id);

            $('#contentdata').html(data);

            var str = '<li folder_id="0" folder_name="My Documents" class="breadCrumb mix_all_list">' +
                '<a id="bread_0" href="javascript:void(0);" folder_id=0 folder_name="My Documents"' +
                ' class="breadCrumb mix_all" onclick="homeFolder();updateFolderStatus(0);" title="Home">Home</a></li>';

            $('#folderBreadCrumb').children().html(str);
            if (folder_id == 0) {
                $('#folderBreadCrumb').hide();
                $('#docdata').removeClass("docdataCls");
            } else {
                $('#folderBreadCrumb').show();
                $('#docdata').addClass("docdataCls");
            }
            if ($('#imp_info').attr('activity_open') == 1) {
                $('.page-sidebar-menu').trigger('click');
                $('#view_detail_btn').trigger('click');
                $('#imp_info').attr('activity_open', 0)
            }
            $('#document_info').attr('folder_level', current_folder_level);
            if (current_folder_level == 5) {
                $('#addFolderButton').hide();
            } else {
                $('#addFolderButton').show();
            }

            if (current_folder_level === 5) {
                $('#new_folder_btn').hide();
            } else {
                $('#new_folder_btn').show();
            }

            $('input[name="usr_upload_dir"]').val($('.breadCrumb.lastElem').attr('data-path'));

        }
    });

}

function homeFolder() {

    parentFolderInfoJSON(null, null, null, []);

    $('#document_info').attr("open_folder_ajax", "0");

    $.ajax({
        type: 'POST',
        url: the_ajax_script.myajaxurl,
        data: {
            action: 'home_folder',
            folder_id: 0
        },
        success: function (data) {

            $('#document_info').attr({
                'folder_parent': 0,
                'current_folder_id': 0,
                'folder_level': 0
            });

            $('#contentdata').html(data);

            $('#folderBreadCrumb').hide();

            $('#document_info').removeAttr('step-1');
            $('#document_info').removeAttr('step-2');
            $('#document_info').removeAttr('step-3');
            $('#document_info').removeAttr('step-4');
        }
    });

}

function updateFolderStatus(flag) {

    if (flag == 0) {
        $('#document_info').attr('current_folder_name', 'My Documents');
        $('#document_info').attr('current_folder_name_tmp', 'My Documents');
    } else {
        $('#document_info').attr({
            'current_folder_name': $.trim(flag),
            'current_folder_name_tmp': $.trim(flag),
            'folder_parent': $.trim(flag),
            'current_folder_id': $.trim(flag).replace(/\s/g, ''),
        });
    }
}

function parentFolderInfoJSON(id = null, name = null, parent_id = null, jsonObj = []) {

    item = {};
    item ["folder_id"] = id;
    item ["folder_name"] = name;
    item ["parent_id"] = parent_id;

    jsonObj.push(item);

}



