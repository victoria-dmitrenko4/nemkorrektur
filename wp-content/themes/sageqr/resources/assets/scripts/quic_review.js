var IN_CURRENT_FOLDER_ON_SHARETAB = '';
var xhr = "";
$('#trashaction').hide();
$('#favourite_btn').on('click', function (event) {
    var title = $('.page_bar>table').find('tr').eq(0).find('td>h4>b').text();
    var action = $(this).attr('data-original-title');
    if (action == 'Add star') {
        var url = base_url + "/document/index/addStarred";
    }
    else {
        var url = base_url + "/document/index/removeStarred";
    }
    var document_id = $(this).attr('data-document_id');
    var version_id = $(this).attr('data-version_id');
    var data_starred_id = $(this).attr('data-starred_id');
    var ref_div_id = $(this).attr('ref-div-id');
    var ref_div_id_second_index = ref_div_id.split("_")[1];
    var dataa = {document_id: document_id, user_id: user_id, version_id: version_id};
    if ($('#imp_info').attr('favourite_ajax') == 0) {
        $('#imp_info').attr('favourite_ajax', 1)
        $.post(url, dataa, function (data) {
            if (action == 'Add star') {
                $('#favourite_btn i').removeClass('icon-star').addClass('glyphicon glyphicon-star');
                $('#favourite_btn').attr('data-original-title', 'Remove star');
                $("#doc_name_" + document_id).parent().find('#doc_view').attr('data-starred_id', '1');
                $('#grid_' + ref_div_id_second_index).attr('is_starred', '1');
                $('#list_' + ref_div_id_second_index).attr('is_starred', '1');
            }
            else {
                $('#favourite_btn i').removeClass('glyphicon glyphicon-star').addClass('icon-star');
                $('#favourite_btn').attr('data-original-title', 'Add star');
                $("#doc_name_" + document_id).parent().find('#doc_view').attr('data-starred_id', '');
                $('#' + ref_div_id).attr('is_starred', '0');
                if ($.trim(title) == "Starred") {
                    $('#grid_' + ref_div_id_second_index).remove();
                    $('#list_' + ref_div_id_second_index).remove();
                    $('.doc_action').hide();
                    blankMsgStarred();
                }
            }
            getactivity(document_id, 'D');
            setTimeout(function () {
                $('#imp_info').attr('favourite_ajax', 0)
            }, 1000);
        });
    }
});
$('#share_btn').on('click', function (event) {
//    $('#shared_user').html("");
    $.ajax({
        type: "POST",
        url: base_url + "/document/index/sharebody",
        data: {},
        beforeSend: function () {
        },
        success: function (data) {
            $('#share_modal_body').html(data);
            $('#share_submit').hide();
            $('#cancel_share').hide();
            $('#share_done').show();
            $('#save_changes').hide();
            $('#cancelBtn').hide();
            document_id = $('#share_btn').attr('data-document_id');
            version_id = $('#share_btn').attr('data-version_id');
            var document_title = $('#share_btn').attr('data-document_title');
            $('#info_on_share').attr('current_selected_document_id', document_id);
            $('#info_on_share').attr('current_selected_version_id', version_id);
            get_shared_user(document_id);
            listSharedUser(document_id,version_id);


            $('#rename_document_title').keypress(function (e) {
                if (e.which == 13) {
                    $('#rename_submit').trigger('click');
                }
            });
        }
    });
});
$('#uploadrevision').on('click', function (event) {
    var document_id = $(this).attr('data-document_id');
    var document_title = $(this).attr('data-document_title');

    $('#revision_document_id').val(document_id);

});
$('#uploadrevision').bind('fileuploadsubmit', function (e, data) {
    var document_id = $('#revision_document_id').val();
    var version_no = $('#version_no').val();
    data.formData = {revision_document_id: document_id, version_no: version_no};
});

$('#view_btn').on('click', function () {
    var params = [
        'height=' + screen.height,
        'width=' + screen.width,
        'fullscreen=yes' // only works in IE, but here for completeness
    ].join(',');
    window.open("", "popup_window", params);
    $.post(base_url + "/document/index/checkIdentity", function (data) {
        if (data.success != 1 && $('#imp_info').attr('logout_click') == 0) {
            window.location.href = $('#document_info').attr("logout_url");
        } else {
            //console.log(base_url);
            var document_id = $('#view_btn').attr('data-document_id');
            var document_title = $('#view_btn').attr('data-document_title');
            var version_no = $('#view_btn').attr('data-version');
            var document_type = $('#view_btn').attr('data-document_type');
            var document_path = $('#view_btn').attr('data-document_path');
            var shared_document = 0;
            if ($('.page-bar>table>tbody>tr>td>h4').text().trim() == 'Shared with me') {
                var shared_document = 1;
            }
            var params = [
                'height=' + screen.height,
                'width=' + screen.width,
                'fullscreen=yes' // only works in IE, but here for completeness
            ].join(',');

            var dataa = {document_id: document_id, document_title: document_title, version_no: version_no, document_type: document_type, document_path: document_path, shared_document: shared_document};
            $.post(base_url + "/document/index/getToken", dataa, function (data) {

//        console.log(data);
                var p_url = PROOFING_URL;
                if (data.document_content_type == 'video') {
                    var p_url = PROOFING_URL.replace("pdf", "video");
                } else if (data.document_content_type == 'html') {
                    var p_url = PROOFING_URL.replace("pdf", "html");
                }

                var newWin = window.open(p_url + '?firstParam=1&t=' + data.token, 'popup_window', params);
//                var newWin = window.open(p_url + '?firstParam=1&t=' + data.token, 'new', params);
//                var newWin = window.open(p_url + '?firstParam=1&t=' + data.token, '_blank', params);



                if (!newWin || newWin.closed || typeof newWin.closed == 'undefined')
                {
                    bootbox.alert("Popup Blocker is enabled! Please add this site to your exception list.");

                }
            }).success(function () {

            });
        }
    });
});

$('#logout').on('click', function () {

    try {
        $.jStorage.flush();
        return true;
    } catch (exception) {
        return true;
    }
});

$('#restore_btn').on('click', function () {
    var current_type = $(this).attr('current_type');
    if (current_type == "file") {
        restoreFiles();
        return true;
    }
    var document_id = $(this).attr('data-document_id');
    var version_id = $(this).attr('data-version_id');
    var folder_id = $(this).attr('folder_id');
    var dataa = {document_id: document_id, version_id: version_id, current_type: current_type, folder_id: folder_id};
    $.post(base_url + "/document/index/restore", dataa, function (data) {
        //for grid update
        if (current_type == "file") {
            $('.doc_div_' + document_id).remove();
            //for list update
            $('#listTable > tbody > tr[data-document_id=' + document_id + ']').remove();
        } else {
            $('#folder_' + folder_id).remove();
            deleteFolderRow(folder_id);
        }
        $('.page-sidebar-menu').trigger('click');
        if ($('#imp_info').attr('activity_open') == 1) {
            $('.page-sidebar-menu').trigger('click');
            $('#view_detail_btn').trigger('click');
            $('#imp_info').attr('activity_open', 0)
        }
        blankMsgTrash();
    });
});

$('#delete_forever_btn').on('click', function () {
    var current_type = $(this).attr('current_type');
    if (current_type == "file") {
        deleteFiles();
        return true;
    }

    var document_id = $(this).attr('data-document_id');
    var version_id = $(this).attr('data-version_id');
    var document_path = $(this).attr('data-document_path');
    var folder_id = $(this).attr('folder_id');

    $('.doc_div_' + document_id).children().html('<table style="width: 100%;height: 80%;text-align:center;"><tbody><tr><td><img class="img-responsive" id="doc_view" src="' + base_url + '/img/106.GIF" alt="" style="width:auto;display:inline;"></td></tr></tbody></table>')
    $('.doc_div_' + document_id).children().removeClass('add_border');

    $('#list_' + document_id).children().eq(0).prepend('<span class="delete_loader"><img class="img-responsive" id="doc_view" src="' + base_url + '/img/105.gif" alt="" style="width:auto;display:inline;"></span>')
    $('#list_' + document_id).css('color', '');
    $('#list_' + document_id).css('background-color', "");
//    return;
    var dataa = {document_id: document_id, version_id: version_id, document_path: document_path, folder_id: folder_id, current_type: current_type};
    $.post(base_url + "/document/index/deleteForever", dataa, function (data) {
        if (current_type == "file") {
            //for grid update
            $('.doc_div_' + document_id).fadeOut('slow');
            //for list update
            $('#listTable > tbody > tr[data-document_id=' + document_id + ']').fadeOut('slow');
            $('.doc_span').hide();
            setTimeout(function () {
                $('.doc_div_' + document_id).remove();
                $('#listTable > tbody > tr[data-document_id=' + document_id + ']').remove();
//                deleteRow(document_id)
                var document_length = $('#uploaded_files').children().length;
            }, 1000);
        } else {
            $('.doc_span').hide();
            //for grid update
            $('#file_bar').hide();
            $('#folder_bar').hide();
            $('#folder_' + folder_id).remove();
            //for list update
            //$('#listTable > tbody > tr[data-document_id='+document_id+']').remove();
            deleteFolderRow(folder_id);
            if ($('#imp_info').attr('activity_open') == 1) {
                $('.page-sidebar-menu').trigger('click');
                $('#view_detail_btn').trigger('click');
                $('#imp_info').attr('activity_open', 0)
            }
        }
        blankMsgTrash();
        getSpaceInfo();
    });
});


$('#expiry_btn').on('click', function (event) {
    var form = $("#form_expiry");
    form.validate().resetForm();
    form[0].reset();
    var document_id = $(this).attr('data-document_id');
    var u = new Date().toString().match(/([-\+][0-9]+)\s/)[1];
    var clientTimeZone = u.substring(0, 3) + ':' + u.substring(3, u.length);
    var time_zone = clientTimeZone;
    dataa = {document_id: document_id, time_zone: time_zone};
    $.post(base_url + "/document/index/getDocument", dataa, function (data) {
        $("#expiry_date").val(data.expiry_date);
        $("#never_expire").val(data.never_expire);
        if (data.never_expire == 1) {
            $('#uniform-never_expire>span').addClass("checked");
            $('.form_datetime').attr('disabled', 'disabled');
            $('#expiry_date').attr('disabled', 'disabled');
            $('.date-set').attr('disabled', 'disabled');
            $('.input-group-btn').addClass("pointer_no");
        } else {
            $('#never_expire').parent().eq(0).removeClass("checked");
            $('.input-group-btn').removeClass("pointer_no");
        }

    });
});



$('#never_expire').on('click', function () {


    if ($(this).val() == 1) {
        //$("#expiry_date").val("");
        $('.form_datetime').attr('disabled', 'disabled');
        $('#expiry_date').attr('disabled', 'disabled');
        $('.date-set').attr('disabled', 'disabled');
        $('.input-group-btn').addClass("pointer_no");
    }
    else {

        $('.form_datetime').removeAttr('disabled');
        $('#expiry_date').removeAttr('disabled');
        $('.date-set').removeAttr('disabled');
        $('.input-group-btn').removeClass("pointer_no");
    }

});

$(".logTime").each(function (index) {
//  var d = moment.unix($( this ).text()).startOf('day').fromNow();
    var UNIX_timestamp = $(this).text();
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = month + ' ' + date + ', ' + year + ' - ' + hour + ' ' + min + ' Hrs';
    $(this).next('span').text(time);
});

$('#cancel_share').on('click', function (event) {

    $('#share_submit').hide();
    $('#cancel_share').hide();
    $('#share_done').show();

});
$('.shareAction').live('change', function () {
    $('#share_done').hide();
    $('#shareUser').hide();


    var share_id = $(this).attr('data-share_id');
    var share_to = $(this).attr('share_to');
    var sharing_forward = $(this).attr('data-sharing_forward');
//    console.log(sharing_forward);
    var download = $(this).attr('data-download');
    var share_action = $(this).val();
    $('#sharing_forward').val(sharing_forward);
    $('#download').val(download);
    if (sharing_forward == 1) {
        $('#uniform-sharing_forward>span').addClass("checked");
    }
    if (download == 1) {
        $('#uniform-download>span').addClass("checked");
    }
    $('#save_changes').attr('data-share_id', share_id);
    $('#save_changes').attr('share_to', share_to);
    $('#save_changes').attr('data-share_action', share_action);
    $('#save_changes').show();
    $('#cancelBtn').show();
    //$('#save_changes').attr('data-sharing_forward',share_id);
    //$('#save_changes').attr('data-download',share_action);
});

$('#save_changes').live('click', function () {
    var share_id = $(this).attr('data-share_id');
    var share_action = $(this).attr('data-share_action');
    var sharing_forward = $('#sharing_forward').val();
    var download = $('#download').val();
    if($('.add_background').length>0){
        var share_to = $(this).attr('share_to');
        saveChanges_forFolder(sharing_forward,share_id,share_action,download,share_to);
        return true;
    }
    var version_id = $('#share_btn').attr('data-version_id');

    var doc_data = [];
    if ($('#grid_btn>i').hasClass('icon-grid')) {
        if ($('.add_border_onlist').length > 1) {
            $('.add_border_onlist').each(function () {
                var document_id = $(this).attr("id").split("_")[1];
                var doc_object = {document_id: document_id}
                doc_data.push(doc_object);
            });
            saveChanges_forMulti(doc_data, share_id, share_action, sharing_forward, download);
            return true;
        }
    } else {
        if ($('.add_border').length > 1) {
            $('.add_border').each(function () {
                var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
                var doc_object = {document_id: document_id}
                doc_data.push(doc_object);
            });
            saveChanges_forMulti(doc_data, share_id, share_action, sharing_forward, download);
            return true;
        }
    }

    $('.slimScrollDiv').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');


    var dataa = {version_id: version_id, share_id: share_id, share_action: share_action, sharing_forward: sharing_forward, download: download};
    $.post(base_url + "/document/index/updateShare", dataa, function (data) {

    }).success(function () {
        $('.page-loading').remove();
        $('#share_done').show();
        $('#shareUser').show();
        $('#save_changes').hide();
        $('#cancelBtn').hide();
    });
});

$('.shareDelete').live('click', function () {
    var share_id = $(this).attr('data-share_id');
    var share_to = $(this).attr('share_to');
    bootbox.confirm("Are you sure you wish to delete share.", function (result) {
        if (result) {
            if($('.add_background').length>0){
                deleteShare_forFolder(share_id,share_to);
                return true;
            }
            var doc_data = [];
            if ($('#grid_btn>i').hasClass('icon-grid')) {
                if ($('.add_border_onlist').length > 1) {
                    $('.add_border_onlist').each(function () {
                        var document_id = $(this).attr("id").split("_")[1];
                        var version_id = $(this).attr("data-version_id");
                        var doc_object = {document_id: document_id, version_id: version_id}
                        doc_data.push(doc_object);
                    });
                    deleteShare_forMulti(doc_data, share_id);
                    return true;
                }
            } else {
                if ($('.add_border').length > 1) {
                    $('.add_border').each(function () {
                        var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
                        var version_id = $(this).parent().eq(0).attr("data-version_id");
                        var doc_object = {document_id: document_id, version_id: version_id}
                        doc_data.push(doc_object);
                    });
                    deleteShare_forMulti(doc_data, share_id);
                    return true;
                }
            }
            $('#shareUser').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
            var version_id = $('#share_btn').attr('data-version_id');
            var document_id = $('#share_btn').attr('data-document_id');
            var dataa = {version_id: version_id, share_id: share_id};
            $.post(base_url + "/document/index/deleteShare", dataa, function (data) {

            }).success(function () {
                $('#share_user_' + share_id).remove();
                $('.page-loading').remove();
                getactivity(document_id, 'D');
            });

        }
    });

});

$('.cross_btn').live('click', function () {
    $(".template-upload").remove();
});

$('.sidemenu').live('click', function () {
    $('#folder_move_open').hide();
    $('#listContent').fadeOut();
    $('#document_info').attr("folder_level", 0);
    $('#document_info').attr('folder_parent', 0);
    $('#document_info').attr('current_folder_id', 0);
    $('#document_info').attr('current_folder_name', 'My Documents');
    $('#document_info').attr('current_folder_name_tmp', 'My Documents');

    $('#addFolderButton').show();
//    return;
    $('#folderBreadCrumb').hide();
    $('#docdata').removeClass("docdataCls");
    $('#folder_bar').hide();
    if ($('#document_info').attr('stop_page_click_ajax') == 0) {
        $('#document_info').attr('stop_page_click_ajax', 1);
        try {
            xhr.abort();
        } catch (e) {

        }
        $('#sticky-wrapper').show();
        $('.page-sidebar-menu li').each(function () {
            $(this).removeClass('active open');
        });
        $(this).addClass('active open');
        var title = $(this).find('a').text();
        if ($.trim(title) == "Support / Feedback") {
            $('.page_bar>table').find('tr').eq(0).find('td>h4>b').text("");
        } else {
            $('.page_bar>table').find('tr').eq(0).find('td>h4>b').text(title);
        }
        Metronic.unblockUI($('#document_pages'));

        if ($.trim(title) == "Trash") {
            $('#empty_trash').fadeIn('slow');
            $('#imp_info').attr('current_tab', 'trash');
        } else {
            $('#empty_trash').fadeOut('slow');
            $('#imp_info').attr('current_tab', 'loading');
            $('#delete_count').fadeOut();

        }
        //$('#fileaction').css('opacity',0);
        //$('#trashaction').css('opacity',0);
        var url = $(this).find('a').attr('data-href');
        if ($('#grid_btn>i').hasClass('icon-grid')) {
            var list = "block";
            var grid = "none";
        } else {
            var list = "none";
            var grid = "block";
        }
        var data_object = {list: list, grid: grid}
        Metronic.startPageLoading();
        $.post(url, data_object, function (data) {

        }).success(function (data) {

            $('.doc_span').hide();
            $('.disabled_icon').hide();
            $('.disable_icon_star').hide();
            $('#docdata').html(data);
            if ($('#imp_info').attr('activity_open') == 1) {
                $('.page-sidebar-menu').trigger('click');
                $('#view_detail_btn').trigger('click');
                $('#imp_info').attr('activity_open', 0)
            }
            try {
//                if ($.jStorage.get("view") == 'list') {
                if ($('#grid_btn>i').hasClass('icon-grid')) {
                    $('#grid_btn>i').removeClass('icon-list').addClass('icon-grid');
//                        $("#gridContent").toggle();

                    setTimeout(function () {
//                        $("#listContent").toggle();
                        Metronic.stopPageLoading();
                    }, 150);

                }
//                else if ($.jStorage.get("view") == 'grid') {
                else {
                    $('#grid_btn>i').removeClass('icon-grid').addClass('icon-list');
                    Metronic.stopPageLoading();
                }
                setWidthForSticky();
                initDocumentTableOnAdd();
                if (InMobile) {
                    short_filename();
                }
                getFileSize();
                return true;
            } catch (exception) {
                return true;
            }
        });
    }

    //Detect Android IOS
    if (InMobile) {
        $('#respSideCon').slideUp();
    }
});
function listSharedUser(document_id,version_id) {
    var dataa = {document_id: document_id,version_id: version_id};
    $.post(base_url + "/document/index/listSharedUser", dataa, function (data) {
        var msg = '';
        var emailArr=[];
        //$('#shared_user').html('');
        var download = 0;
        $.each(data, function (index, value) {
            download = value.download;
            var select1 = (value.share_action == 'Comment') ? 'selected="selected"' : '';
            var select2 = (value.share_action == 'View') ? 'selected="selected"' : '';
            //console.log(value);
            var access = (value.access == 'view') ? '<span class="label label-sm label-success label-mini">Can View </span>' : '<span class="label label-sm label-danger label-mini">Can Comment </span>';

            var sharedUser = value.shared_users_emails.split(',');

            $.each(sharedUser, function (i, v) {
                if(emailArr.indexOf(v)==-1){
                    msg += '<div class="row">';
                    msg += '<div class="col-md-12 user-info" id="share_user_' + value.share_id + '"><img alt="" src="' + base_url + '/assets/admin/layout/img/avatar.png" class="img-responsive imgWidth"/><div class="details"><div style="color: #5B9BD1;">' + v + '</div></div>';
                    msg += '<div class="pull-right"><select class="bs-select form-control input-small dropdown-btn shareAction" data-style="btn-success" share_to=' + value.shared_to + ' data-share_id="' + value.share_id + '" data-sharing_forward="' + value.sharing_forward + '" data-download="' + value.download + '"><option value="Comment" ' + select1 + '>Can comment</option><option value="View" ' + select2 + '>Can view</option></select>';
                    msg += '<a href="javascript:;" class="btn btn-xs red shareDelete" share_to=' + value.shared_to + ' data-share_id="' + value.share_id + '" style="margin-left:20px;margin-top: 5px;"><i class="fa fa-times"></i></a></div></div>';
                    msg += '</div>';
                    emailArr.push(v);
                }
            })



            // msg+='<div class="row"><div class="col-md-6 user-info"><img alt="" src="'+base_url+'/assets/admin/layout/img/avatar.png" class="img-responsive imgWidth"/><div class="details"><div><a href="javascript:;">Robert Nilson </a><span class="label label-sm label-success label-mini">Approved </span></div></div></div><div class="col-md-6 user-info"><img alt="" src="'+base_url+'/assets/admin/layout/img/avatar.png" class="img-responsive imgWidth"/><div class="details"><div><a href="javascript:;">Lisa Miller </a> <span class="label label-sm label-danger label-mini">Pending </span></div></div></div></div>';
            //msg+='<p><span>'+value.display_name+'</span>(<span>'+value.email+'</span>)(<span>'+value.access+'</span>)</p>'
        });
        if (msg == "") {
            msg = "<span style='font-style:italic;color:#ABABAE;'>No user accessed.</span>"
            $('#shared_user').html(msg);
            $('#uniform-download').children().eq(0).removeClass('checked');
            $('#uniform-download').children().eq(0).children().eq(0).val(0);
        } else {
            setTimeout(function () {
                $('#shared_user').html("");
                $('#shared_user').append(msg);
                $('#uniform-download').addClass('focus');
                if (download == 0) {
                    $('#uniform-download').children().eq(0).removeClass('checked');
                    $('#uniform-download').children().eq(0).children().eq(0).val(0);
                } else {
                    $('#uniform-download').children().eq(0).addClass('checked');
                    $('#uniform-download').children().eq(0).children().eq(0).val(1);
                }
                ComponentsDropdowns.init();
            }, 1000);
        }
    });
}

function share_done() {
    var doc_data = [];
    var download = $("#download").val();

    if($('.add_background').length>0){
        share_done_forFolder(download);
        return true;
    }

    if ($('#grid_btn>i').hasClass('icon-grid')) {
        if ($('.add_border_onlist').length > 1) {
            $('.add_border_onlist').each(function () {
                var document_id = $(this).attr("id").split("_")[1];
                var doc_object = {document_id: document_id}
                doc_data.push(doc_object);
            });
            share_done_forMulti(doc_data, download);
            return true;
        }
    } else {
        if ($('.add_border').length > 1) {
            $('.add_border').each(function () {
                var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
                var doc_object = {document_id: document_id}
                doc_data.push(doc_object);
            });
            share_done_forMulti(doc_data, download);
            return true;
        }
    }


    var document_id = $('#share_btn').attr('data-document_id');
    $('#share_modal_body').html("");
    var dataa = {user_id: user_id, document_id: document_id, download: download};
    $.post(base_url + "/document/index/sharedone", dataa, function (data) {

    }).success(function () {

    });
}

function submit_share() {
    $('#shareUser').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
    var document_id = $('#share_btn').attr('data-document_id');
    var version_id = $('#share_btn').attr('data-version_id');
    var share_email = $("#input1").val();
    var share_action = $("#share_action").val();
    var sharing_forward = $("#sharing_forward").val();
    var download = $("#download").val();
    var document_title = $('#share_btn').attr('data-document_title');
    var document_type = $('#share_btn').attr('data-document_type');
    var document_content_type = $('#share_btn').attr('document_content_type');

    var version_no = $('#share_btn').attr('data-version');
    var document_path = $('#share_btn').attr('data-document_path');
    var shared_document = 0;
    if ($('.page-bar>table>tbody>tr>td>h4').text().trim() == 'Shared with me') {
        shared_document = 1;
    }
    var anybody = $("#anybody").val();
    var dataa = {document_content_type: document_content_type, version_no: version_no, document_path: document_path, shared_document: shared_document, user_id: user_id, document_id: document_id, share_email: share_email, share_action: share_action, version_id: version_id, sharing_forward: sharing_forward, download: download, document_title: document_title, document_type: document_type,anybody:anybody};
    $.post(base_url + "/document/index/share", dataa, function (data) {

    }).success(function () {
        //$('#share_modal').modal('hide');
        $('.page-loading').remove();
        $('#share_submit').hide();
        $('#cancel_share').hide();
        $('#share_done').show();
        $('#save_changes').hide();
        $('#cancelBtn').hide();
        listSharedUser(document_id,version_id);
        get_shared_user(document_id);
        $('.removeEmail').each(function () {
            $(this).trigger('click');
        });

        getactivity(document_id, 'D');
    });
}



function resetEmailBar() {
    $('.removeEmail').each(function () {
        $(this).trigger('click');
    });
    $('#cancel_share').fadeOut();
    $('#share_submit').fadeOut();
    $('#invalid_email-error').hide();
}

function get_shared_user(document_id) {
    $("#select2_sample5").select2("val", "");
    var dataa = {document_id: document_id};
    $.post(base_url + "/document/index/listUser", dataa, function (data) {
        $("#select2_sample5").select2({
            tags: data,
            selectOnBlur: true,
        });
        setTimeout(function () {
            $('#s2id_select2_sample5').on('click', function () {
                $('#share_submit').show();
                $('#cancel_share').show();
                $('#share_done').hide();
            });
            $('#select2_sample5').on('change', function () {
                $(this).valid();
            });
        }, 200);
    });
    Metronic.initSlimScroll('.scroller');
}

function rename_submit() {
    $('#renameDocument').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
    var document_id = $('#rename_document_id').val();
    var document_title = $('#rename_document_title').val();
    var version_id = $('#rename_btn').attr('data-version_id');
    var dataa = {document_id: document_id, document_title: document_title, version_id: version_id};

    $.post(base_url + "/document/index/rename", dataa, function (data) {
    })
        .success(function () {
            $('.page-loading').remove();
            $('#rename_modal').modal('hide');
            //For grid update
            $('#doc_name_' + document_id).find('#label').text(document_title);
            $('#doc_name_' + document_id + '>h5').attr('data-original-title', document_title);
            //For list update
            $('#listTable > tbody > tr[data-document_id=' + document_id + ']').find('#label').text(document_title);

            $('#rename_btn').attr('data-document_title', document_title);
            $('#grid_' + document_id).attr('data-document_title', document_title);
            $('#list_' + document_id).attr('data-document_title', document_title);
            getactivity(document_id, 'D');
        });
}

function expirySubmit() {
    $('#expiryDocument').append('<div class="page-loading" style="display:block !important;z-index:9;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
    var document_id = $('#expiry_btn').attr('data-document_id');
    var version_id = $('#expiry_btn').attr('data-version_id');
    var never_expire = $("#never_expire").val();
    var expiry_date1 = $("#expiry_date").val().replace("-", "");
    var u = new Date().toString().match(/([-\+][0-9]+)\s/)[1];
    var clientTimeZone = u.substring(0, 3) + ':' + u.substring(3, u.length);
    var expiry_date = (expiry_date1 != '') ? expiry_date1 + " " + clientTimeZone : '';
    var dataa = {document_id: document_id, version_id: version_id, never_expire: never_expire, expiry_date: expiry_date};
    $.post(base_url + "/document/index/setExpiry", dataa, function (data) {
//        console.log(data);
    }).success(function () {
        getactivity(document_id, 'D');
        $('.page-loading').remove();
        $('#expiry_modal').modal('hide');
    });
}

function getdetails(document_id) {
//    alert(document_id);
//    var document_id = $('#favourite_btn').attr('data-document_id');
    var dataa = {document_id: document_id};

    $.post(base_url + "/document/index/getdetails", dataa, function (data) {

    }).success(function (data) {
        $('#details_list').html(data);
        $('#details_size').text(filesize($('#details_size').text(), {round: 0}));
    });
}

function getactivity(document_folder_id, type) {
//    var document_id = $('#favourite_btn').attr('data-document_id');
    if ($('#grid_btn>i').hasClass('icon-grid')) {
        if ($('.add_border_onlist').length > 1) {
            $('#details_list, #activity_list').html('<ul class="feeds list-items"><li class="sel">Select a file to view its details.</li></ul>');
            return false;
        }
    } else {
        if ($('.add_border').length > 1) {
            $('#details_list, #activity_list').html('<ul class="feeds list-items"><li class="sel">Select a file to view its details.</li></ul>');
            return false;
        }
    }

    var start = 0;
    var limit = ACTIVITY_LIST_LENGTH;
    var dataa = {document_folder_id: document_folder_id, type: type, start: start, limit: limit};

    $.post(base_url + "/document/index/getactivity", dataa, function (data) {

    }).success(function (data) {
        $('#activity_list').html(data);
        $(".logTime").each(function (index) {

//				  var d = moment.unix($( this ).text()).startOf('day').fromNow();
//				  $( this ).next('span').text(d);
            var UNIX_timestamp = $(this).text();
            var a = new Date(UNIX_timestamp * 1000);
            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            var time = month + ' ' + date + ', ' + year + ' - ' + hour + ' ' + min + ' Hrs';
            $(this).next('span').text(time);
        });
    });
}

function loadMoreActivity(document_id) {
    var start = $('#imp_info').attr('activity_list_start');
    var limit = $('#imp_info').attr('activity_list_length');
    if ($('#imp_info').attr('activity_list_ajax') == 0) {
        $('#imp_info').attr('activity_list_ajax', 1);
        var dataa = {document_id: document_id, start: start, limit: limit};

        $.post(base_url + "/document/index/getmoreactivity", dataa, function (data) {

        }).success(function (data) {
            $('#list_activity_item').append(data);
            $(".logTime").each(function (index) {
                var UNIX_timestamp = $(this).text();
                var a = new Date(UNIX_timestamp * 1000);
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                var year = a.getFullYear();
                var month = months[a.getMonth()];
                var date = a.getDate();
                var hour = a.getHours();
                var min = a.getMinutes();
                var sec = a.getSeconds();
                var time = month + ' ' + date + ', ' + year + ' - ' + hour + ' ' + min + ' Hrs';
                $(this).next('span').text(time);
            });
            start = parseInt(start) + parseInt(limit);
            $('#imp_info').attr('activity_list_start', start);
            $('#imp_info').attr('activity_list_ajax', 0);
        });
    }
}

function getFileSize() {

    $('.list_file_size').each(function () {
        //console.log($(this).text().trim());
        if ($(this).attr('folder') == 1) {
            $(this).html("<span style='margin-left:10px;'>-</span>");
        } else {
            var final_file_size = filesize($(this).text().trim(), {round: 0});
            var final_file_size_amount = final_file_size.split(" ")[0];
            var final_file_size_unit = final_file_size.split(" ")[1];
            if (final_file_size_unit == "kB")
                var filtered_file_size = final_file_size_amount + " KB";
            else
                var filtered_file_size = final_file_size;

            $(this).text(filtered_file_size);
        }
    });

}

var PersonalFormValidation = function () {

    // basic validation
    var handleValidation1 = function () {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var form1 = $('#user_profile_info');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            messages: {
                select_multi: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                }
            },
            rules: {
                name: {
                    minlength: 2,
                    required: true
                },
                new_password: {
                    minlength: 6,
                },
                confirm_password: {
                    minlength: 6,
                    equalTo: "#new_confirm_password"
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                Metronic.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
                submit_form('user_profile_info');
            }
        });


    }

    var handleWysihtml5 = function () {
        if (!jQuery().wysihtml5) {

            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["../../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {

            handleWysihtml5();
            handleValidation1();

        }

    };

}();
function load_page(page) {
    $('#folder_move_open').hide();
    $('.page-sidebar-menu li').each(function () {
        $(this).removeClass('active open');
    });
    var url = $('#' + page).attr('data-href');
    Metronic.startPageLoading();
    $.post(url, function (data) {
    }).success(function (data) {
        Metronic.stopPageLoading();
        $('#sticky-wrapper').hide();
        $('#docdata').html(data);
        PersonalFormValidation.init();
    });

}

function submit_form(form_id) {
    $.ajax({
        type: "POST",
        url: $('#' + form_id).attr('action'),
        data: $('#' + form_id).serialize(),
        beforeSend: function () {
        },
        success: function (data) {
            Messenger().post({
                message: "Changes has been saved.",
                hideAfter: 1
            });

        }, error: function () {
            alert('There was an error connecting to the server. Please try again.');
        }
    });
}

function check_exist(file_type, file_path, callback) {
    $.ajax({
        type: "POST",
        url: base_url + "/document/index/checkfileexist",
        data: {
            file_path: file_path,
            file_type: file_type
        },
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (typeof callback == "function")
                callback(data);
        }, error: function () {
            alert('There was an error connecting to the server. Please try again.');
        }
    });
}

function set_date_time(time_zone, expiry_date, created_at, modified_at, callback) {
    $.ajax({
        type: "POST",
        url: base_url + "/document/index/setdatetime",
        data: {time_zone: time_zone, expiry_date: expiry_date, created_at: created_at, modified_at: modified_at},
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (typeof callback == "function")
                callback(data);
        }, error: function () {
            console.log('There was an error connecting to the server. Please try again.');
        }
    });
}

function validate_email(callback) {
    var all_emails_correct = 1
    var email_arr = $('#input1').val().split(',');
    $.each(email_arr, function (i) {
        var email = email_arr[i];
        var check = validateEmail(email);
        if (check == 0 && email != "") {
            all_emails_correct = 0;
//                $('.label-info').css('border-bottom', 'solid 2px #797979')
            $(".label-info:contains('" + email + "')").css("border-bottom", "solid 2px red");
        }

    });
    if (all_emails_correct == 1) {
        $('#invalid_email').val("1");
    } else {
        $('#invalid_email').val("");
    }
    callback();
}

function validateEmail(email) {
    var regex = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;
    if (!regex.test($.trim(email))) {
        return 0;
    } else {
        return 1;
    }
}

function empty_trash() {
//    var content = $('#uploaded_files').children().length;
    var content = parseInt($('#uploaded_files').children().length) + parseInt($('#created_folder').children().length);
    if (content == "0") {
        bootbox.alert(MSG_ON_DELETE_DOC_ON_TRASH_NODOC);

    } else {

        bootbox.dialog({
            message: '<span> ' + MSG_ON_DELETE_DOC_ON_TRASH + ' </span>',
            title: "",
            buttons: {
                success: {
                    label: "Yes",
                    className: "red",
                    callback: function () {
                        Metronic.blockUI({
                            message: 'Deleting <span id="current_no"></span> of <span id="total_count"></span>',
                            target: $('#document_pages'),
                            overlayColor: 'none',
                            cenrerY: false,
                            boxed: true,
                        });
                        $('.blockElement').addClass('custom_fixed_block_ui_bar');
                        deleteDocuments();
                    }
                },
                danger: {
                    label: "No",
                    className: "blue",
                    callback: function () {
                    }
                }
            }
        });
    }
}

function deleteDocuments() {
    // $("[id^=grid_]").each(function () {
    var document_length = parseInt($('#uploaded_files').children().length) + parseInt($('#created_folder').children().length);
    var current_total_count = $('#imp_info').attr('total_count');
    var current_no = $('#imp_info').attr('current_no');
    var current_tab = $('#imp_info').attr('current_tab');
    if (current_tab == "trash") {
        if (document_length > 0) {
            $('#current_no').text(current_no);
            $('#total_count').text(current_total_count);
            $('#delete_count').fadeIn();
            var folderBarLength = $('#created_folder').children().length;
            if (folderBarLength > 0) {
                var element = $('#created_folder').children().eq(0);
                var folder_id = element.attr('folder_id');
                $('#folder_' + folder_id).children().html('<table style="width: 100%;height: 40px;text-align:center;"><tbody><tr><td><img class="img-responsive" id="doc_view" src="' + base_url + '/img/105.gif" alt="" style="width:auto;display:inline;"></td></tr></tbody></table>')
                $('#folder_list_' + folder_id).children().eq(0).prepend('<span class="delete_loader"><img class="img-responsive" id="doc_view" src="' + base_url + '/img/105.gif" alt="" style="width:auto;display:inline;"></span>')
                $('.nameBar').width($('.nameBar').width() - 50 + "px");
                $('.nameBar').css('margin-top', '5px');
                var dataa = {folder_id: folder_id};
                xhr = $.ajax({
                    type: "POST",
                    url: base_url + "/document/index/deleteFolderForever",
                    data: dataa,
                    success: function (data) {
                        $('#folder_' + folder_id).fadeOut('slow');
                        $('#folder_list_' + folder_id).fadeOut('slow');
                        //for list update
//                        $('#created_folder > tbody > tr[folder_id=' + folder_id + ']').fadeOut('slow');
                        $('.doc_span').hide();
                        setWidthForSticky();
                        setTimeout(function () {
                            $('#folder_' + folder_id).remove();
                            deleteFolderRow(folder_id);
//                            $('#listFolder').children().eq(1).prepend('<tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">No data available</td></tr>')
                            current_no = parseInt(current_no) + 1;
                            $('#imp_info').attr('current_no', current_no);
                            deleteDocuments();
                            blankMsgTrash();
                            getSpaceInfo();
                        }, 1000);
                    }
                });
            } else {
                var element = $('#uploaded_files').children().eq(0);
                var document_id = element.attr('data-document_id');
                var version_id = element.attr('data-version_id');
                var document_path = element.attr('data-document_path');
                $('.doc_div_' + document_id).children().html('<table style="width: 100%;height: 80%;text-align:center;"><tbody><tr><td><img class="img-responsive" id="doc_view" src="' + base_url + '/img/106.GIF" alt="" style="width:auto;display:inline;"></td></tr></tbody></table>')
                $('.doc_div_' + document_id).children().removeClass('add_border');

                $('#list_' + document_id).children().eq(0).prepend('<span class="delete_loader"><img class="img-responsive" id="doc_view" src="' + base_url + '/img/105.gif" alt="" style="width:auto;display:inline;"></span>')
                $('.nameBar').width($('.nameBar').width() - 50 + "px");
                $('#list_' + document_id).css('color', '');
                $('#list_' + document_id).css('background-color', "");
                var folder_id = "";
                var current_type = "file";
                var dataa = {document_id: document_id, version_id: version_id, document_path: document_path, folder_id: folder_id, current_type: current_type};
                xhr = $.ajax({
                    type: "POST",
                    url: base_url + "/document/index/deleteForever",
                    data: dataa,
                    success: function (data) {
                        $('.doc_div_' + document_id).fadeOut('slow');
                        //for list update
                        $('#listTable > tbody > tr[data-document_id=' + document_id + ']').fadeOut('slow');
                        $('.doc_span').hide();
                        setWidthForSticky();
                        setTimeout(function () {
                            $('.doc_div_' + document_id).remove();
                            deleteRow(document_id)
                            current_no = parseInt(current_no) + 1;
                            $('#imp_info').attr('current_no', current_no);
                            deleteDocuments();
                            blankMsgTrash();
                            getSpaceInfo();
                        }, 1000);
                    }
                });
            }
        } else {
            $('#delete_count').fadeOut();
//            $('#gridContent').append('<div class="alert alert-warning" style="display:block" "=""><strong>Empty!</strong> No documents are found in trash.</div>');
            $('#imp_info').attr('total_count', '');
            $('#imp_info').attr('current_no', 1);
            Metronic.unblockUI($('#document_pages'));
            if ($('#listTable').children().eq(1).children().length == 0) {
                $('#listTable').children().eq(1).html('<tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">No documents are found in trash.</td></tr>')
                $('#listTable').click(function () {
                    $('#listTable').children().eq(1).html('<tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">No documents are found in trash.</td></tr>')
                });
            }
        }
    }

}


function openAvtivityView() {
    if ($('#imp_info').attr('activity_open') == 0)
        $('#imp_info').attr('activity_open', 1);
    else
        $('#imp_info').attr('activity_open', 0);
}

function showHideAddBAr() {
    if ($('#imp_info').attr('showhideaddbar_open') == 0) {
        $('#imp_info').attr('showhideaddbar_open', 1);
        $('#addShowHideBar').addClass('btn-group');
        $('#addShowHideBar').addClass('open');
    } else {
        $('#imp_info').attr('showhideaddbar_open', 0);
        $('#addShowHideBar').removeClass('open');
    }
}

function showAddFolderBAr() {
    $('#folder_move_open').hide();
    $('#folder_bar').hide();
    $('#files_bar').hide();
    var box = bootbox.dialog({
        message: '<span style="font-size:145%"> New Folder </span><span> <input class="form-control" style="width:269px;margin-top:15px;" name="folder_name" id="input_folder_name" onkeyup="checkForBlank(this.value);" value="Untitled Folder" /> </span>',
        title: "",
        className: "custom",
        buttons: {
            danger: {
                label: "Cancel",
                className: "",
                callback: function () {
                }
            },
            success: {
                label: "Create",
                className: "blue cls_folder_name",
                callback: function () {
                    addFolder();
                }
            }
        }
    });
    box.on("shown.bs.modal", function () {
        $('#input_folder_name').select();
        $('#input_folder_name').keypress(function (e) {
            var key = e.which;
            if (key == 13) {
                addFolder();
                $('.bootbox-close-button').trigger('click');
                return false;
            }
        });
    });


}

function checkForBlank(folder_name) {
    if ($.trim(folder_name) == "") {
        $('.cls_folder_name').attr('disabled', 'disabled');
    } else {
        $('.cls_folder_name').removeAttr('disabled');
    }
    $('#folder_name').val(folder_name);
}

function addFolder() {
    Metronic.startPageLoading();
    $('#emptyMsg').hide();
    $('#emptyMsg').remove();
    var folder_name = $('#input_folder_name').val();
    Messenger().post({
        message: "Folder is creating.",
        hideAfter: 1
    });
    $.ajax({
        type: "POST",
        url: base_url + "/document/index/addfolder",
        data: {folder_name: folder_name, user_id: user_id, parent: $('#document_info').attr('folder_parent')},
        success: function (data) {
//            console.log(data);
//            var str = '<div id="folder_'+data.folder_id+'" folder_id="'+data.folder_id+'" folder_name="'+folder_name+'" user_id="'+user_id+'" parent_id="'+$('#document_info').attr('folder_parent')+'" style="display: block; opacity: 1; " class="col-md-3 col-sm-4 col-xs-6 mix category_1 doc_div_1443 mix_all">\n\
//                            <div id="folder-bar" class="mix-inner folder folder-double-click">\n\
//                                <div for="folder" id="folder_info_wrapper" class="folder_info_wrapper">\n\
//                                    <span class="folder_icon"><i id="folder_image" class="fa fa-folder doc_icon custom_folder"></i></span>\n\
//                                    <span id="folder_name" for="folder" class="folder_name">'+folder_name+'</span>\n\
//                                </div>\n\
//                            </div>\n\
//                        </div>';
//            $('#created_folder').prepend(str);
//
//            var td_width_0 = $('.doc_row').eq(0).children().eq(0).width();
//            var td_width_1 = $('.doc_row').eq(0).children().eq(1).width();
//            var td_width_2 = $('.doc_row').eq(0).children().eq(2).width();
//            var td_width_3 = $('.doc_row').eq(0).children().eq(3).width();
//            var list_str = '<tr class="doc_row odd" role="row">\n\
//                                <td class="show_tool" width="'+td_width_0+'px">\n\
//                                    <div class="nameBar" style="width: '+td_width_0+'px; float: left; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">\n\
//                                        <span><i class="fa fa-folder doc_icon" style="color:#676767 !important;font-size:139%;"></i></span>\n\
//                                        <span id="label">'+folder_name+'</span>\n\
//                                    </div>\n\
//                                </td>\n\
//                                <td class="show_tool" width="'+td_width_1+'px">bilal abbasi</td>\n\
//                                <td class="show_tool sorting_1" width="'+td_width_2+'px">28 Apr 2016 17 40 Hrs</td>\n\
//                                <td class="list_file_size show_tool" folder="1" width="'+td_width_3+'px"><span style="margin-left:10px;">-</span></td>\n\
//                            </tr>';
//            try{
//                $('#listFolder').children().eq(1).prepend(list_str);
//            } catch(e){
//                alert('not')
//            }

            $("#page_title_bar").find('h4').html("<b>My Documents</b>");
            $('.sidemenu').removeClass('active');
            $('.sidemenu').removeClass('open');

            $('.sidemenu').eq(0).addClass('active');
            $('.sidemenu').eq(0).addClass('open');

            Metronic.stopPageLoading();
            $('.doc_span').hide();
            $('.disabled_icon').hide();
            $('.disable_icon_star').hide();
            $('#docdata').html(data);
            if ($('#imp_info').attr('activity_open') == 1) {
                $('.page-sidebar-menu').trigger('click');
                $('#view_detail_btn').trigger('click');
                $('#imp_info').attr('activity_open', 0)
            }
            try {
                if ($.jStorage.get("view") == 'list') {
                    $('#grid_btn>i').removeClass('icon-list').addClass('icon-grid');

                    $("#gridContent").toggle();
                    $("#listContent").toggle();
                }
                else if ($.jStorage.get("view") == 'grid') {
                    $('#grid_btn>i').removeClass('icon-grid').addClass('icon-list');
                    //$("#gridContent").toggle();
                    //$("#listContent").toggle();
                }
                setWidthForSticky()
                initDocumentTableOnAdd();
                getFileSize();
                open_folder_onDBClick();
                return true;
            } catch (exception) {
                return true;
            }
        }, error: function () {
        }
    });
}

//function open_folder_onDBClick_old() {
//    $('.tooltip').hide();
//    $('.folder-double-click, .doc_row').on('dblclick', function (e) {
//
//        var myObj = e.target;
//        if ($(myObj).attr('for') == 'files') {
//            return false;
//        }
//        if ($('#grid_btn>i').hasClass('icon-grid')) {
//            var folder_id = $(this).attr("folder_id");
//        } else {
//            var folder_id = $(this).parent().attr("folder_id");
//        }
//        var folder_level = $('#document_info').attr('folder_level');
//        var current_folder_level = parseInt(folder_level) + 1;
//        var title = $("#page_title_bar").find('h4').text();
////        alert(title);
//        $('#document_info').attr('current_folder_name', $.trim($(this).text()));
//        $('#document_info').attr('current_folder_name_tmp', $.trim($(this).text()));
//        if ($('#document_info').attr("double_click_intrash") == 0) {
//            $('#document_info').attr("double_click_intrash", 1)
//            if ($.trim(title) == "Trash") {
//                trashFolderRestoreBox(folder_id);
//            } else {
//                openFolder(folder_id, current_folder_level);
//                $('#document_info').attr("double_click_intrash", 0)
//            }
//        }
//    })
//}

function open_folder_onDBClick() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Windows Phone/i.test(navigator.userAgent)) {
        // alert('-1');

        $('.tooltip').hide();
        var touchtime = 0;
        $(".folder-double-click").on("click", function (e) {
            // alert(0);

            var elem = $(this);
            if (touchtime == 0) {
                // alert(1);

                // set first click
                touchtime = new Date().getTime();
            } else {
                // alert(2);

                // compare first click to this click and see if they occurred within double click threshold
                if (((new Date().getTime()) - touchtime) < 600) {
                    // double click occurred
                    // alert(3);
                    folderOpen(e, elem)
                    touchtime = 0;
                } else {
                    // not a double click so set as a new first click
                    touchtime = new Date().getTime();
                }
            }
        });
    } else {
        $('.tooltip').hide();
        $('.folder-double-click').on('dblclick', function (e) {
            var elem = $(this);
            folderOpen(e, elem);
        })
    }
}

function open_folder_onDBClickOnShare() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Windows Phone/i.test(navigator.userAgent)) {
        $('.tooltip').hide();
        var touchtime = 0;
        $(".folder-double-click-onshare").on("click", function (e) {
            var elem = $(this);
            if (touchtime == 0) {
                // set first click
                touchtime = new Date().getTime();
            } else {
                // compare first click to this click and see if they occurred within double click threshold
                if (((new Date().getTime()) - touchtime) < 600) {
                    // double click occurred
                    folderOpenOnShare(e, elem)
                    touchtime = 0;
                } else {
                    // not a double click so set as a new first click
                    touchtime = new Date().getTime();
                }
            }
        });
    } else {
        $('.tooltip').hide();
        $('.folder-double-click-onshare').on('dblclick', function (e) {
            var elem = $(this);
            folderOpenOnShare(e, elem);
        })
    }
}

function folderOpen(e, elem) {
    $("#page_title_bar").find("h4").addClass("customForMob")
    var myObj = e.target;
    if ($(myObj).attr('for') == 'files') {
        return false;
    }
    if ($('#grid_btn>i').hasClass('icon-grid')) {
        var folder_id = elem.attr("folder_id");
    } else {
        var folder_id = elem.parent().attr("folder_id");
    }
    //alert(folder_id);
    var folder_level = $('#document_info').attr('folder_level');
    var current_folder_level = parseInt(folder_level) + 1;
    var title = $("#page_title_bar").find('h4').text();
//        alert(title);
    $('#document_info').attr('current_folder_name', $.trim(elem.text()));
    $('#document_info').attr('current_folder_name_tmp', $.trim(elem.text()));
    if ($('#document_info').attr("double_click_intrash") == 0) {
        $('#document_info').attr("double_click_intrash", 1)
        if ($.trim(title) == "Trash") {
            trashFolderRestoreBox(folder_id);
        } else {
            openFolder(folder_id, current_folder_level);
            $('#document_info').attr("double_click_intrash", 0)
        }
    }
}

function folderOpenOnShare(e, elem) {
    $("#page_title_bar").find("h4").addClass("customForMob")
    var myObj = e.target;
    if ($(myObj).attr('for') == 'files') {
        return false;
    }
    if ($('#grid_btn>i').hasClass('icon-grid')) {
        var folder_id = elem.attr("folder_id");
        var user_id = elem.attr("user_id");
        var root_folder_id_on_share = elem.attr("root_folder_id_on_share");
    } else {
        var folder_id = elem.parent().attr("folder_id");
        var user_id = elem.parent().attr("user_id");
        var root_folder_id_on_share = elem.parent().attr("root_folder_id_on_share");
    }
    try{
        if(root_folder_id_on_share==undefined){
            IN_CURRENT_FOLDER_ON_SHARETAB = IN_CURRENT_FOLDER_ON_SHARETAB;
        } else {
            IN_CURRENT_FOLDER_ON_SHARETAB = root_folder_id_on_share;
        }
    } catch(e){
        IN_CURRENT_FOLDER_ON_SHARETAB = IN_CURRENT_FOLDER_ON_SHARETAB;
    }
    var folder_level = $('#document_info').attr('folder_level');
    var current_folder_level = parseInt(folder_level) + 1;
    var title = $("#page_title_bar").find('h4').text();
//        alert(title);
    $('#document_info').attr('current_folder_name', $.trim(elem.text()));
    $('#document_info').attr('current_folder_name_tmp', $.trim(elem.text()));
    if ($('#document_info').attr("double_click_intrash") == 0) {
        $('#document_info').attr("double_click_intrash", 1)
        if ($.trim(title) == "Trash") {
            trashFolderRestoreBox(folder_id);
        } else {
            openFolderOnShare(folder_id, current_folder_level,user_id);
            $('#document_info').attr("double_click_intrash", 0)
        }
    }
}

function trashFolderRestoreBox(folder_id) {
    var box = bootbox.dialog({
        message: "To view this folder, you'll need to restore it from your trash.",
        title: "<b>This folder is in your trash</b>",
        className: "custom_trash",
        buttons: {
            danger: {
                label: "Cancel",
                className: "",
                callback: function () {
                    $('#document_info').attr("double_click_intrash", 0)
                }
            },
            success: {
                label: "Restore",
                className: "blue cls_folder_name",
                callback: function () {
                    var document_id = "";
                    var version_id = "";
                    var current_type = "folder";
                    var dataa = {document_id: document_id, version_id: version_id, current_type: current_type, folder_id: folder_id};
                    $.post(base_url + "/document/index/restore", dataa, function (data) {
                        //for grid update
                        if (current_type == "file") {
                            $('.doc_div_' + document_id).remove();
                            //for list update
                            $('#listTable > tbody > tr[data-document_id=' + document_id + ']').remove();
                        } else {
                            $('#folder_' + folder_id).remove();
                            deleteFolderRow(folder_id);
                        }
                        $('.page-sidebar-menu').trigger('click');
                        blankMsg();
                    });
                    $('#document_info').attr("double_click_intrash", 0)
                }
            }
        }
    });

    $('.close').click(function () {
        $('#document_info').attr("double_click_intrash", 0)
    });
}

function openFolder(folder_id, current_folder_level) {
    if ($('#document_info').attr("open_folder_ajax") == "0") {
        $('#document_info').attr("open_folder_ajax", "1")
        $('#folder_bar').hide();
        $('#files_bar').hide();
        var url = base_url + "/document/index/folderdata";
        Metronic.startPageLoading();
        $.post(url, {folder_id: folder_id}, function (data) {
        }).success(function (data) {
            $('#document_info').attr('folder_parent', folder_id);
            $('#document_info').attr('current_folder_id', folder_id);
            Metronic.stopPageLoading();
            $('#folder_bar').hide();
            $('#files_bar').hide();
            $('.doc_span').hide();
            $('.disabled_icon').hide();
            $('.disable_icon_star').hide();
            $('#docdata').html(data);
            var folder_parent_JSON = $('#folder_parent_info').text();
            var folder_parent_JSON_arr = $.parseJSON(folder_parent_JSON);
            setGoBackDir(folder_parent_JSON_arr,null);
            var str = '<li folder_id=0 folder_name="My Documents" class="breadCrumb mix_all_list"><a id="bread_0" href="javascript:void(0);" folder_id=0 folder_name="My Documents"  class="breadCrumb mix_all" onclick="openFolder(0,0);updateFolderStatus(0);" title="Home">Home</a><i class="fa fa-angle-right"></i></li>';
            var folder_parent_count = folder_parent_JSON_arr.length;
            var folder_parent_count_org = parseInt(folder_parent_count) - 1;
            var incrIndex = 0
            $.each(folder_parent_JSON_arr, function (i) {
                incrIndex = i + 1;
                var link_with_name = '';
                var last_arrow = '';

                if (i == folder_parent_count_org) {
                    link_with_name = '<h8 class="breadCrumb">' + folder_parent_JSON_arr[i]['folder_name'] + '</h8>';
                    last_arrow = "";
                    var lastCLass="lastElm"
                } else {
                    link_with_name = '<h8 class="breadCrumb"><a class="breadCrumb mix_all" folder_id=' + folder_parent_JSON_arr[i]['folder_id'] + ' folder_name="' + folder_parent_JSON_arr[i]['folder_name'] + '" id="bread_' + incrIndex + '" href="javascript:void(0);" onclick="openFolder(' + folder_parent_JSON_arr[i]['folder_id'] + ',' + incrIndex + ');updateFolderStatus(' + incrIndex + ');">' + folder_parent_JSON_arr[i]['folder_name'] + '</h8></a>';
                    last_arrow = '<i  class="fa fa-angle-right custom_right_icon breadCrumb mix_all" folder_id=' + folder_parent_JSON_arr[i]['folder_id'] + ' folder_name="' + folder_parent_JSON_arr[i]['folder_name'] + '"></i>';
                    var lastCLass=""
                }

                str += '<li folder_id=' + folder_parent_JSON_arr[i]['folder_id'] + ' folder_name="' + folder_parent_JSON_arr[i]['folder_name'] + '" class="breadCrumb mix_all_list '+lastCLass+'" style="font-size:14px;" title="' + folder_parent_JSON_arr[i]['folder_name'] + '">' + link_with_name + last_arrow + '</i></li>'
            });
            $('#folderBreadCrumb').children().html(str);
            if (folder_id == 0) {
                $('#folderBreadCrumb').hide();
                $('#docdata').removeClass("docdataCls");
            } else {
                $('#folderBreadCrumb').show();
                $('#docdata').addClass("docdataCls");
            }
            if ($('#imp_info').attr('activity_open') == 1) {
                $('.page-sidebar-menu').trigger('click');
                $('#view_detail_btn').trigger('click');
                $('#imp_info').attr('activity_open', 0)
            }
            $('#document_info').attr('folder_level', current_folder_level);
            if (current_folder_level == 5) {
                $('#addFolderButton').hide();
            } else {
                $('#addFolderButton').show();
            }
            try {
                if ($.jStorage.get("view") == 'list') {
                    $('#grid_btn>i').removeClass('icon-list').addClass('icon-grid');

                    $("#gridContent").toggle();
                    $("#listContent").toggle();
                }
                else if ($.jStorage.get("view") == 'grid') {
                    $('#grid_btn>i').removeClass('icon-grid').addClass('icon-list');
                    //$("#gridContent").toggle();
                    //$("#listContent").toggle();
                }
                if (InMobile) {
                    short_filename();
                }
                initDocumentTableOnAdd();
                return true;
            } catch (exception) {
                $('#document_info').attr("open_folder_ajax", "0")
                return true;
            }
        });
    }
}

function openFolderOnShare(folder_id, current_folder_level,user_id) {
    if(folder_id==0){
        $('.sidemenu').eq(1).trigger("click");
        return false;
    }
    if ($('#document_info').attr("open_folder_ajax") == "0") {
        $('#document_info').attr("open_folder_ajax", "1")
        $('#folder_bar').hide();
        $('#files_bar').hide();
        var url = base_url + "/document/index/folderdataonshare";
        Metronic.startPageLoading();
        $.post(url, {folder_id: folder_id,user_id:user_id}, function (data) {
        }).success(function (data) {
            $('#document_info').attr('folder_parent', folder_id);
            $('#document_info').attr('current_folder_id', folder_id);
            Metronic.stopPageLoading();
            $('#folder_bar').hide();
            $('#files_bar').hide();
            $('.doc_span').hide();
            $('.disabled_icon').hide();
            $('.disable_icon_star').hide();
            $('#docdata').html(data);
            var folder_parent_JSON = $('#folder_parent_info').text();
            var folder_parent_JSON_arr = $.parseJSON(folder_parent_JSON);
            setGoBackDir(folder_parent_JSON_arr,user_id);
            var indexForRootFolderInShare = folder_parent_JSON_arr.findIndex(p => p.folder_id == IN_CURRENT_FOLDER_ON_SHARETAB);
            folder_parent_JSON_arr.splice(0, indexForRootFolderInShare);
            folder_parent_JSON_arr
            var str = '<li class="breadCrumb"><a id="bread_0" href="javascript:void(0);" folder_id=0 folder_name="Share with me"  class="breadCrumb mix_all" onclick="openFolderOnShare(0,0,'+user_id+');updateFolderStatus(0);" title="Share with me">Share with me</a><i class="fa fa-angle-right"></i></li>';
            var folder_parent_count = folder_parent_JSON_arr.length;
            var folder_parent_count_org = parseInt(folder_parent_count) - 1;
            var incrIndex = 0
            $.each(folder_parent_JSON_arr, function (i) {
                incrIndex = i + 1;
                var link_with_name = '';
                var last_arrow = '';

                if (i == folder_parent_count_org) {
                    link_with_name = '<h8 class="breadCrumb">' + folder_parent_JSON_arr[i]['folder_name'] + '</h8>';
                    last_arrow = "";
                } else {
                    link_with_name = '<h8 class="breadCrumb"><a class="breadCrumb mix_all" folder_id=' + folder_parent_JSON_arr[i]['folder_id'] + ' folder_name="' + folder_parent_JSON_arr[i]['folder_name'] + '" id="bread_' + incrIndex + '" href="javascript:void(0);" onclick="openFolderOnShare(' + folder_parent_JSON_arr[i]['folder_id'] + ',' + incrIndex + ','+user_id+');updateFolderStatus(' + incrIndex + ');">' + folder_parent_JSON_arr[i]['folder_name'] + '</h8></a>';
                    last_arrow = '<i class="fa fa-angle-right custom_right_icon breadCrumb mix_all" folder_id=' + folder_parent_JSON_arr[i]['folder_id'] + ' folder_name="' + folder_parent_JSON_arr[i]['folder_name'] + '"></i>';
                }

                str += '<li class="breadCrumb" style="font-size:14px;" title="' + folder_parent_JSON_arr[i]['folder_name'] + '">' + link_with_name + last_arrow + '</i></li>'
            });
            $('#folderBreadCrumb').children().html(str);
            if (folder_id == 0) {
                $('#folderBreadCrumb').hide();
                $('#docdata').removeClass("docdataCls");
            } else {
                $('#folderBreadCrumb').show();
                $('#docdata').addClass("docdataCls");
            }
            if ($('#imp_info').attr('activity_open') == 1) {
                $('.page-sidebar-menu').trigger('click');
                $('#view_detail_btn').trigger('click');
                $('#imp_info').attr('activity_open', 0)
            }
            $('#document_info').attr('folder_level', current_folder_level);
            if (current_folder_level == 5) {
                $('#addFolderButton').hide();
            } else {
                $('#addFolderButton').show();
            }
            try {
                if ($.jStorage.get("view") == 'list') {
                    $('#grid_btn>i').removeClass('icon-list').addClass('icon-grid');

                    $("#gridContent").toggle();
                    $("#listContent").toggle();
                }
                else if ($.jStorage.get("view") == 'grid') {
                    $('#grid_btn>i').removeClass('icon-grid').addClass('icon-list');
                    //$("#gridContent").toggle();
                    //$("#listContent").toggle();
                }
                if (InMobile) {
                    short_filename();
                }
                initDocumentTableOnAdd();
                return true;
            } catch (exception) {
                $('#document_info').attr("open_folder_ajax", "0")
                return true;
            }
        });
    }
}

function updateFolderStatus(flag) {
    if (flag == 0) {
        $('#document_info').attr('current_folder_name', 'My Documents');
        $('#document_info').attr('current_folder_name_tmp', 'My Documents');
    } else {
        $('#document_info').attr('current_folder_name', $.trim($("#bread_" + flag).text()));
        $('#document_info').attr('current_folder_name_tmp', $.trim($("#bread_" + flag).text()));
    }
}

function triggerHome() {
    $('.sidemenu').eq(0).trigger('click');
    $('#document_info').attr('folder_level', 0);

}


function tableSorting(id, index, order) {
    var oTable = $('#listTable').dataTable();
    oTable.fnSort([index, order]);
    if (order == "asc") {
        $('#' + id).attr('onclick', "tableSorting('" + id + "'," + index + ",'desc')");
    } else {
        $('#' + id).attr('onclick', "tableSorting('" + id + "'," + index + ",'asc')");
    }
//        $('#'+id).
}

function rename_prev_processing(element) {
    var validator = $("#folder_rename").validate();
    validator.resetForm();
    var folder_id = $('#' + element).attr('folder_id');
    var folder_name = $('#folder_' + folder_id).find('#folder_name').text();
    $('#rename_folder_title').val(folder_name);
    $('#rename_folder_id').val(folder_id);
}

function folder_rename_submit() {
    $('#renameFolder').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');

    var folder_id = $('#rename_folder_id').val();
    var folder_title = $('#rename_folder_title').val();
    var dataa = {folder_id: folder_id, folder_title: folder_title, folder_parent: $('#document_info').attr("folder_parent")};

    $.post(base_url + "/document/index/renamefolder", dataa, function (data) {

    })
        .success(function (retdata) {
            $('.page-loading').remove();
            $('#folder_rename_modal').modal('hide');
            $('#folder_bar').hide();
            Metronic.stopPageLoading();
            $('.doc_span').hide();
            $('.disabled_icon').hide();
            $('.disable_icon_star').hide();
            $('#docdata').html(retdata);
            if ($('#imp_info').attr('activity_open') == 1) {
                $('.page-sidebar-menu').trigger('click');
                $('#view_detail_btn').trigger('click');
                $('#imp_info').attr('activity_open', 0)
            }
            try {
                if ($.jStorage.get("view") == 'list') {
                    $('#grid_btn>i').removeClass('icon-list').addClass('icon-grid');

                    $("#gridContent").toggle();
                    $("#listContent").toggle();
                }
                else if ($.jStorage.get("view") == 'grid') {
                    $('#grid_btn>i').removeClass('icon-grid').addClass('icon-list');
                    //$("#gridContent").toggle();
                    //$("#listContent").toggle();
                }
                setWidthForSticky()
                initDocumentTableOnAdd();
                getFileSize();
                open_folder_onDBClick();
                return true;
            } catch (exception) {
                return true;
            }
//                $('#folder_'+folder_id).find('#folder_name').text(folder_title);
//                $('#folder_list_'+folder_id).find('#label').text(folder_title);



        });
}

$('#folder_delete_btn').on('click', function () {
    var folder_id = $(this).parent().attr('folder_id');
    var dataa = {folder_id: folder_id};
    bootbox.confirm("Are you sure you wish to delete this folder.\n When you delete this folder, the document inside it will be removed from dashboard of all users that have access to the document inside it.", function (result) {
        if (result) {
            $.post(base_url + "/document/index/deletefolder", dataa, function (data) {
                $('.doc_span').hide();
                //for grid update
                $('#file_bar').hide();
                $('#folder_bar').hide();
                $('#folder_' + folder_id).remove();
                //for list update
                //$('#listTable > tbody > tr[data-document_id='+document_id+']').remove();
                deleteFolderRow(folder_id);
                if ($('#imp_info').attr('activity_open') == 1) {
                    $('.page-sidebar-menu').trigger('click');
                    $('#view_detail_btn').trigger('click');
                    $('#imp_info').attr('activity_open', 0)
                }
                blankMsg();
            });
        }
    });
    if (InMobile) {
        $('body').addClass('disableScroll');
        $('.bootbox-close-button, button[data-bb-handler~="cancel"], button[data-bb-handler="confirm"]').on('click',function(){
            $('body').removeClass('disableScroll');
        });
    }
});

if (InMobile) {
    $('#trash_rev, #rename_btn, #folder_rename_wrapper').click(function(){
        $('body').addClass('disableScroll');
        $('.bootbox-close-button, button[data-bb-handler~="cancel"], button[data-bb-handler="confirm"], button[data-dismiss="modal"], button[id="rename_submit"]').on('click',function(){
            $('body').removeClass('disableScroll');
        });
    });
}



function deleteFolderRow(folder_id) {
    var settings = $('#listFolder').dataTable().fnSettings();
    var count = settings.aoData.length;
    for (var i = 0; i < count; i++)
    {
        var theNode = settings.aoData[i].nTr;
        //console.log(theNode.attributes[4]);
        if (theNode.attributes['folder_id'].nodeValue == folder_id)
        {
            $("#listFolder").dataTable().fnDeleteRow(i);
            break;
        }
    }
    setWidthForSticky();
}

$('#move_btn').on('click', function () {
    if ($('#folder_move_open').css('display') == "block") {
        return;
    }
    $('#move_folder_now').attr('disabled', 'disabled');
    $('#disableLayer').show();
    $('#move_folder_now').addClass('move_btn_disabled');
    // $('#folder_move_bar').hide();
    var document_id = $(this).attr('data-document_id');
    $('#folder_move_open').fadeIn();
    $('#file_bar').trigger('click');
    try {
        setTimeout(
            function () {
                $("#move_btn").tooltip("hide");
                var here = 1;
            }, 200)
    } catch (e) {
    }
    getFolderListStr($('#document_info').attr('current_folder_id'), function (str, parentName, grandParentId) {
        $('#dd_menu').html(str);
        $('#move_folder_now').attr('onclick', "moveFlderNow(" + $('#document_info').attr('current_folder_id') + ")");
        $('#folder_tile_in_move').text(parentName);
        $('#folder_tile_in_move').attr('title', parentName);
        var parent = $('#document_info').attr('current_folder_id');
        if ($('#document_info').attr('current_folder_id') != 0) {
            $('#prev_folder').fadeIn();
            $('#prev_folder').attr('onclick', "loadPrevFolder(" + grandParentId + ")");
        } else {
            $('#prev_folder').fadeOut();
        }

        folderClickMoveTime()
    });
});

function getFolderListStr(parent, callback) {
//    var allFolderData = $('#index_info').text();
//    var allFolderDataArr = $.parseJSON(allFolderData);
//    var str = '';
//    $.each(allFolderDataArr,function(i){
//        if(allFolderDataArr[i]['parent_id']==parent){
//            str+='<li title="'+allFolderDataArr[i]['folder_name']+'" class="folder_list_cls" folder_id = "'+allFolderDataArr[i]['folder_id']+'" folder_name="'+allFolderDataArr[i]['folder_name']+'" parent_id="'+allFolderDataArr[i]['parent_id']+'">'+allFolderDataArr[i]['folder_name']+'</li>';
//        }
//    });
//    if(str=="") {
//        str+='<li  class="no_folder_found"> No Folder found.</li>';
//    }
    var data_object = {parent: parent}
    var data_object1 = {folder_id: parent}
    $.post(base_url + "/document/index/getfolderlist", data_object, function (str, parentName) {
        $.post(base_url + "/document/index/getfolderdetails", data_object1, function (object) {
//            console.log(object.folderDetails.length);
            if (object.folderDetails.length == 0) {
                var parentName = "My Documents";
                var grandParentId = 0;
            } else {
                var parentName = object.folderDetails[0].folder_name;
                var grandParentId = object.folderDetails[0].parent_id;
            }
            callback(str, parentName, grandParentId);
        });
    });
}

function folderClickMoveTime() {
    $('.folder_list_cls').click(function (event) {
        var element = $(this);
        // move button on off
        var document_folder_id = $('#document_info').attr('document_folder_id');
        var current_folder_id = element.attr('folder_id')
        if (document_folder_id == current_folder_id) {
            $('#move_folder_now').attr('disabled', 'disabled');
            $('#disableLayer').show();
            $('#move_folder_now').addClass('move_btn_disabled');
        } else {
            $('#move_folder_now').removeAttr('disabled');
            $('#disableLayer').hide();
            $('#move_folder_now').removeClass('move_btn_disabled');
        }
        var current_folder_name = element.attr('folder_name');
        $('#document_info').attr('current_folder_name', current_folder_name);
        $('#document_info').attr('current_folder_name_tmp', current_folder_name);
        $('#folder_move_bar').fadeIn();
        //        $('.folder_list_cls').removeClass('add_background_light_blue');
        if (element.hasClass('add_background_light_blue')) {
            element.removeClass('add_background_light_blue');
            // move button on off
            var document_folder_id = $('#document_info').attr('document_folder_id');
            var current_folder_id = $('#document_info').attr('current_folder_id');
            if (document_folder_id == current_folder_id) {
                $('#move_folder_now').attr('disabled', 'disabled');
                $('#disableLayer').show();
                $('#move_folder_now').addClass('move_btn_disabled');
            } else {
                $('#move_folder_now').removeAttr('disabled');
                $('#disableLayer').hide();
                $('#move_folder_now').removeClass('move_btn_disabled');
            }
        } else {
            $('.folder_list_cls').removeClass('add_background_light_blue');
            element.addClass('add_background_light_blue');
        }
        $('#move_folder_now').attr('onclick', "moveFlderNow(" + element.attr('folder_id') + ")");
    });

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Windows Phone/i.test(navigator.userAgent)) {
        var touchtime = 0;
        $(".folder_list_cls").on("click", function (e) {
            var element = $(this);
            if (touchtime == 0) {
                // set first click
                touchtime = new Date().getTime();
            } else {
                // compare first click to this click and see if they occurred within double click threshold
                if (((new Date().getTime()) - touchtime) < 800) {
                    // double click occurred
                    openFolderData(element);
                    touchtime = 0;
                } else {
                    // not a double click so set as a new first click
                    touchtime = new Date().getTime();
                }
            }
        });
    } else {
        $('.folder_list_cls').dblclick(function (event) {
            event.preventDefault();
            var element = $(this);
            openFolderData(element);
        });
    }
}

function openFolderData(element) {
    // move button on off
    var document_folder_id = $('#document_info').attr('document_folder_id');
    var current_folder_id = element.attr('folder_id');
    var current_folder_name = element.attr('folder_name');
    $('#document_info').attr('current_folder_id', current_folder_id);
    $('#document_info').attr('current_folder_name', current_folder_name);
    $('#document_info').attr('current_folder_name_tmp', current_folder_name);
    if (document_folder_id == current_folder_id) {
        $('#move_folder_now').attr('disabled', 'disabled');
        $('#disableLayer').show();
        $('#move_folder_now').addClass('move_btn_disabled');
    } else {
        $('#move_folder_now').removeAttr('disabled');
        $('#disableLayer').hide();
        $('#move_folder_now').removeClass('move_btn_disabled');
    }


    $('#move_folder_now').attr('onclick', "moveFlderNow(" + element.attr('folder_id') + ")");
    $('#folder_tile_in_move').text(element.attr('folder_name'));
    $('#folder_tile_in_move').attr('title', element.attr('folder_name'));
    var parent = element.attr('parent_id');
    if (element.attr('folder_name') != 0) {
        $('#prev_folder').fadeIn();

        $('#prev_folder').attr('onclick', "loadPrevFolder(" + parent + ")");
    } else {
        $('#prev_folder').fadeOut();
    }
    getFolderListStr(element.attr('folder_id'), function (str) {
        $("#dd_menu").hide().show("slide", {direction: "right"}, 100);
        $('#dd_menu').html(str);
        folderClickMoveTime()
    });
}

function clickHeading(id) {
    $('#folder_tile_in_move').html('<span class="tooltips" data-placement="bottom" title="You are now ready to move file to root.">\n\
            <div id="myDocumentMainLayer">My Documents</div>\n\
            </span>');
    $('#myDocumentMainLayer').addClass('myDocumentMainLayerBorder');
    $('#folder_move_bar').fadeIn();
    $('.folder_list_cls').removeClass('add_background_light_blue');
    $('#move_folder_now').attr('onclick', "moveFlderNow('0')");
}


function getGrandParent(parent) {
    var allFolderData = $('#index_info').text();
    var allFolderDataArr = $.parseJSON(allFolderData);
    var str = '';
    $.each(allFolderDataArr, function (i) {
        if (allFolderDataArr[i]['parent_id'] == parent) {
            str += '<li class="folder_list_cls" folder_id = "' + allFolderDataArr[i]['folder_id'] + '" folder_name="' + allFolderDataArr[i]['folder_name'] + '" parent_id="' + allFolderDataArr[i]['parent_id'] + '">' + allFolderDataArr[i]['folder_name'] + '</li>';
        }
    });
    if (str == "") {
        str += '<li  class="no_folder_found"> No Folder found.</li>';
    }
    callback(str);
}

function loadPrevFolder(parent) {

    // move button on off
    var document_folder_id = $('#document_info').attr('document_folder_id');
    var current_folder_id = parent;
    $('#document_info').attr('current_folder_id', current_folder_id);
    if (document_folder_id == current_folder_id) {
        $('#move_folder_now').attr('disabled', 'disabled');
        $('#disableLayer').show();
        $('#move_folder_now').addClass('move_btn_disabled');
    } else {
        $('#move_folder_now').removeAttr('disabled');
        $('#disableLayer').hide();
        $('#move_folder_now').removeClass('move_btn_disabled');
    }


    var allFolderData = $('#index_info').text();
    var allFolderDataArr = $.parseJSON(allFolderData);
    var parentArr = $.grep(allFolderDataArr, function (folder) {
        return folder.folder_id == parent
    });
    var grandParent = "";
    if (parentArr == "") {
    } else {
        grandParent = parentArr[0]['parent_id'];
    }
    getFolderListStr(parent, function (str) {
        $("#dd_menu").hide().show("slide", {direction: "left"}, 100);
        $('#dd_menu').html(str);
        folderClickMoveTime();
        if (parent == 0) {
            $('#prev_folder').hide();
            $('#folder_tile_in_move').text("My documents");
            $('#document_info').attr('current_folder_name', 'My documents');
            $('#document_info').attr('current_folder_name_tmp', 'My documents');

//            $('#folder_move_bar').fadeOut();
        } else {
            $('#folder_tile_in_move').text(parentArr[0]['folder_name']);
            $('#document_info').attr('current_folder_name', parentArr[0]['folder_name']);
            $('#document_info').attr('current_folder_name_tmp', parentArr[0]['folder_name']);
            $('#prev_folder').attr('onclick', 'loadPrevFolder(' + grandParent + ')');
        }
        $('#move_folder_now').attr('onclick', 'moveFlderNow(' + parent + ')')
    });
}

$('.doc_span').on('click', function () {
    if ($(this).attr('id') == "file_move_bar") {

    } else {
        $('#folder_move_open').hide();
    }
})

function closeMoveBar() {
    $('#folder_move_open').fadeOut();
}

function moveFlderNow(folder_id) {
    Metronic.blockUI({
        message: 'Moving...',
        target: $('#created_folder'),
        overlayColor: 'none',
        cenrerY: false,
        boxed: true,
    });
    $('#folder_move_open').hide();
    var document_title = $('#file_move_bar').children().eq(0).attr('data-document_title');
    var folder_name = $('#document_info').attr('current_folder_name');
    if (folder_name == "My documents")
        var message = 'The file "' + document_title + '" has been moved to root successfully.'
    else
        var message = 'The file "' + document_title + '" has been moved to "' + folder_name + '" folder successfully.'

    var document_id = $('#file_move_bar').children().eq(0).attr('data-document_id');
    var version_id = $('#file_move_bar').children().eq(0).attr('data-version_id');
    var data_document_title = $('#file_move_bar').children().eq(0).attr('data-document_title');
    var data_object = {folder_id: folder_id, document_id: document_id, version_id: version_id, data_document_title: data_document_title}
    $.post(base_url + "/document/index/movefile", data_object, function (data) {
        var page = $('.sidemenu').closest('.open').children().children().eq(1).text();
        if ($.trim(page) == "My Documents") {
            $('#grid_' + document_id).fadeOut(function () {
                if (data.folderDataList == 0 && $('#created_folder').children().length == 0 && data.currentFolderId != 0) {
                    $('#emptyMsg').html("<strong>Empty!</strong> No documents available for you.");
                    $('#emptyMsg').show();
                }
            });
            deleteRow(document_id);


            setTimeout(function () {
                msg = Messenger().post({
                    message: message,
                    hideAfter: 6,
                    hideOnNavigate: true,
                    showCloseButton: true,
                    id: 'saving-msg'
                })
                Metronic.unblockUI($('#created_folder'));
            }, 1000);

        }

        if ($('#imp_info').attr('activity_open') == 1) {
            $('.page-sidebar-menu').trigger('click');
            $('#view_detail_btn').trigger('click');
            $('#imp_info').attr('activity_open', 0)
        }
        $('.page-sidebar-menu').trigger("click");
    });
}

function whoToShareClick(element) {
    var element = $(element);
    if (element.parent().parent().hasClass('open_custom')) {
        element.parent().parent().removeClass('open_custom');
        element.attr('data-original-title', "Click to show");
    } else {
        element.parent().parent().addClass('open_custom');
        element.attr('data-original-title', "Click to hide")
    }
    element.tooltip("hide");
}



document.ondragstart = function (e) {
    return false

};

var dragging = [];
//function dragStart(event) {
//    if (document.readyState !== 'complete')
//        return;
//    var currentTagret = event.target.id;
//    $('#full_wrapper').hide();
//    $('#dropBar').hide();
//}
//
//function dragEnter(event) {
//    if (document.readyState !== 'complete')
//        return;
//    var currentTagret = event.target.id;
//    $('#full_wrapper').show();
//    $('#dropBar').show();
//}
//
//function dragLeave(event) {
//    if (document.readyState !== 'complete')
//        return;
//    var currentTagret = event.target.id;
//    $('#full_wrapper').hide();
//    $('#dropBar').hide();
//}
//
//function allowDrop(event) {
//    event.preventDefault();
//}
//function drop(event) {
//    if (document.readyState !== 'complete')
//        return;
//    $('#full_wrapper').hide();
//    $('#dropBar').hide();
//}

$(document).bind('dragover', function (e) {
    if (e.target == this) {
        return;
    }
    $('.droptarget').addClass('hover');
}).bind('dragleave', function (e) {
    if (e.originalEvent.pageX != 0) {
        return false;
    }
    e.stopPropagation();
    $('.droptarget').removeClass('hover');
}).bind('drop', function (e) {
    e.preventDefault();
    $('.droptarget').removeClass('hover');
}).bind('drop dragover', function (e) {
    e.preventDefault();
});



var $selection;
var $container
var click = false;
var elementFadeIn = false;
var runSelectFun = true;
var startScroll = true;
function dragDropOnDocument() {
//    return;

    $container = $('body');
    $container.on('mousedown', function (e) {
        if (e.ctrlKey || e.shiftKey) {
            ctrl = true;
        }
        if (e.which == 1) {
            var page = $('.sidemenu').closest('.open').children().children().eq(1).text();
            if ($.trim(page) == "My Documents") {
                if ($(e.target).attr('for') != "files") {
                    return true;
                }
                click = true;
                $selection = $('#draggableElement');
                var click_y = e.pageY;
                var click_x = e.pageX;
                $selection.css({'top': parseInt(click_y) + 20, 'left': parseInt(click_x) + 20});
                $('.files_bar').hide();
                $('#folder_move_open').hide();
                if (ctrl == false) {
                    // for grid
//                    $('#uploaded_files').find('.add_border').removeClass('add_border');
//                    $('[id^=doc_name_]').removeAttr('style');
//                    $('[id^=doc_name_]>h5').removeAttr('style');
//
//                    // for list
//                    $('#listTable > tbody > tr').removeAttr('style');
//                    $('#listTable_wrapper').find('.add_border_onlist').removeClass('add_border_onlist');
                }
            }
        }
    });

    $container.on('mousemove', function (e) {
        if (e.which == 1) {
            if (click == true) {
                $('body').css('cursor', 'move');
                var element = e.target;
                if (elementFadeIn == false) {
                    elementFadeIn = true;
//                    $('#files_bar').hide();
//                    $('#folder_bar').hide();
//                    $('#multiple_file_select_actions').hide();
                    var doc_id = $(element).closest('.dragSelectionEL').attr('data-document_id');
                    if ($('#grid_btn>i').hasClass('icon-list')) {
                        $('#doc_name_' + doc_id).css({'background-color': '#1caf9a'});
                        $('#doc_name_' + doc_id + '>h5').css({'color': 'white'});
                        $('#fileTypeIcon').html($('#doc_name_' + doc_id + '>h5').children().html());
                        $('#dragFileName').html($('#doc_name_' + doc_id + '>h5').children().eq(1).text());
                        $(element).closest('.mix-inner').addClass('add_border');
                        $('.add_border').each(function (i) {
                            $(this).css('opacity', '.4');
                        });
                    } else {
                        runSelectFun = false;
                        $('#list_' + doc_id).addClass('add_border_onlist');
                        $('#list_' + doc_id).css({'background-color': '#1caf9a'});
                        $('#list_' + doc_id).css({'color': 'white'});
                        $('#fileTypeIcon').html($('#doc_name_' + doc_id + '>h5').children().html());
                        $('#dragFileName').html($('#doc_name_' + doc_id + '>h5').children().eq(1).text());
                        $('.add_border_onlist').each(function (i) {
                            $(this).css('opacity', '.4');
                        });
                    }
                }

                if ($('#grid_btn>i').hasClass('icon-list')) {
                    if ($(element).closest(".mix_all").hasClass('folderIcon')) {
                        $('.folder-double-click').removeClass('add_background')
                        $('.folder-double-click').removeClass('add_border')
                        $(element).closest("#folder-bar").addClass("add_background");
                        $(element).closest("#folder_image").addClass("add_folder_background");
                        if ($(element).attr('id') == "folder_name") {
                            $(element).prev().eq(0).children().addClass("add_folder_background");
                        }
                        if ($(element).attr('id') == "folder_info_wrapper") {
                            $(element).children().children().addClass("add_folder_background");
                        }
                    } else if ($(element).hasClass("breadCrumb")) {
                        $(element).parent().eq(0).addClass("parentFolderHover");
                    } else {
                        $('.folder-double-click').removeClass('add_background');
                        $('.folder-double-click').removeClass('add_border');
                        $('.breadCrumb').parent().eq(0).removeClass("parentFolderHover");
                    }
                } else {
                    if ($(element).closest(".mix_all_list").hasClass('folderIcon_list')) {
                        $('.folderIcon_list').removeClass('add_background');
                        $(element).closest(".mix_all_list").addClass("add_background");
                    } else if ($(element).hasClass("breadCrumb")) {
                        $(element).parent().eq(0).addClass("parentFolderHover");
                    } else {
                        $('.folderIcon_list').removeClass('add_background');
                        $(element).closest(".mix_all_list").removeClass('add_background');
                        $('.breadCrumb').parent().eq(0).removeClass("parentFolderHover");
                    }
                }

                $selection.stop(true, true).fadeIn();
                if ($('#grid_btn>i').hasClass('icon-list')) {
                    // for grid
                    if ($('#uploaded_files').find('.add_border').length > 1) {
                        $('#file_move_bar_1').hide();
                        $('#file_move_disabled_1').show();
                        $('#dragFileCount').text($('#uploaded_files').find('.add_border').length);
                        $('#dragFileCount').show();
                    } else {
                        $('#dragFileCount').hide();
                    }
                } else {
                    // for list
                    if ($('#listTable_wrapper').find('.add_border_onlist').length > 1) {
                        $('#dragFileCount').text($('#listTable_wrapper').find('.add_border_onlist').length);
                        $('#dragFileCount').show();
                    } else {
                        $('#dragFileCount').hide();
                    }
                }
                ctrl = false;
                var move_x = e.clientX;
                var move_y = e.clientY;
                $selection.css({
                    'top': parseInt(move_y) + 10,
                    'left': parseInt(move_x) + 10
                });
                if (e.clientY < 100) {
                    if (startScroll) {
                        $('html,body').animate({scrollTop: 0}, 3000);
                        startScroll = false;
                    }
                } else if (($(window).height() - e.clientY) < 100) {
                    if (startScroll) {
                        $('html,body').animate({scrollTop: $(document).height()}, 3000);
                        startScroll = false;
                    }
                } else {
                    $('html,body').stop();
                    startScroll = true;
                }
            }
        }
    })

    $container.on('mouseup', function (e) {
        $('body').css('cursor', 'default');
        if (e.which == 1) {
            try {
                if (elementFadeIn) {
                    e.preventDefault();
                    e.stopPropagation();
                    $('#files_bar').hide();
                    $('#folder_bar').hide();
                    $selection.hide();
                    $('#dragFileCount').hide();
                    elementFadeIn = false
                    click = false;
                    var element = e.target;
                    if ($(element).closest(".mix-inner").hasClass('folder-double-click') || $(element).closest(".mix_all_list").hasClass('folderIcon_list') || $(element).hasClass('breadCrumb') || $(element).closest(".breadCrumb").hasClass("breadCrumb")) {
                        if ($('#grid_btn>i').hasClass('icon-list')) {
                            var folder_id = $(element).closest(".mix_all").attr('folder_id');
                            var folder_name = $(element).closest(".mix_all").attr('folder_name');
                            var fileCount = $('.add_border').length;
                            var filesValuesArr = [];
                            $('.add_border').each(function (i) {
                                var elementInside = $(this).parent().eq(0);
                                var document_id = elementInside.attr('data-document_id');
                                var version_id = elementInside.attr('data-version_id');
                                var data_document_title = elementInside.attr('data-document_title');
                                var fileValuesObject = {document_id: document_id, version_id: version_id, data_document_title: data_document_title};
                                filesValuesArr.push(fileValuesObject)
                            });
                        } else {
                            var folder_id = $(element).closest(".mix_all_list").attr('folder_id');
                            var folder_name = $(element).closest(".mix_all_list").attr('folder_name');
                            var fileCount = $('.add_border_onlist').length;
                            var filesValuesArr = [];
                            $('.add_border_onlist').each(function (i) {
                                var elementInside = $(this);
                                var document_id = elementInside.attr('data-document_id');
                                var version_id = elementInside.attr('data-version_id');
                                var data_document_title = elementInside.attr('data-document_title');
                                var fileValuesObject = {document_id: document_id, version_id: version_id, data_document_title: data_document_title};
                                filesValuesArr.push(fileValuesObject)
                            });
                        }
                        if($(element).closest('.mix_all_list').hasClass("lastElm")){
                            $('.add_border').each(function (i) {
                                $(this).css('opacity', '1');
                            });
                            $('.add_border_onlist').each(function (i) {
                                $(this).css('opacity', '1');
                            });
                        } else {
                            moveMultipleFileToFolder(folder_id, folder_name, fileCount, filesValuesArr);
                        }
//                        $(element).trigger('click');
                    } else {
                        $('.add_border').each(function (i) {
                            $(this).css('opacity', '1');
                        });

                        $('.add_border_onlist').each(function (i) {
                            $(this).css('opacity', '1');
                        });

                    }

                    //custom might be deleted
                    if (ctrl == false) {
                        if ($(e.target).closest("#files_bar").hasClass('fileaction_icon')) {
                        } else {
                            // for grid
//                            $('#uploaded_files').find('.add_border').removeClass('add_border');
//                            $('[id^=doc_name_]').removeAttr('style');
//                            $('[id^=doc_name_]>h5').removeAttr('style');

                            // for list
//                            $('#listTable > tbody > tr').removeAttr('style');
//                            $('#listTable_wrapper').find('.add_border_onlist').removeClass('add_border_onlist');
                        }
                    }
                    if ($('.add_border').length == 1)
                        $('.add_border').trigger("click");


                    // after drop show file bar again
                    if ($('#grid_btn>i').hasClass('icon-grid')) {
                        if ($('.add_border_onlist').length == 0) {
                            $('#files_bar').hide();
                            $('.disabled_icon').hide();
                            $('#multiple_file_select_actions').hide();
                        } else if ($('.add_border_onlist').length == 1) {
                            $('#files_bar').show();
                            $('.doc_span').show();
                            $('.doc_action').show();
                            $('#multiple_file_select_actions').hide();
                        } else {
                            $('#files_bar').hide();
//                            $('.doc_span').show(); //Sprint 3 QR-58 added by Ranjeet
                            $('.disabled_icon').hide();
                            $('#multiple_file_select_actions').show();
                        }
                    }

                    if ($('#grid_btn>i').hasClass('icon-list')) {
                        if ($('.add_border').length == 0) {
                            $('#files_bar').hide();
                            $('.disabled_icon').hide();
                            $('#multiple_file_select_actions').hide();
                        } else if ($('.add_border').length == 1) {
                            $('.add_border').trigger("click");
                        } else {
                            $('#files_bar').hide();
                            $('.disabled_icon').hide();
                            $('#multiple_file_select_actions').show();
                        }
                    }

                }
            } catch (e) {
            }
        }
    });

    $(document).on('mouseup', function (e) {
        $('body').css('cursor', 'default');

        try {

            /*****Prevent deselect file or folder click on disable icon*******/
//            if(!$(e.target).hasClass("enable_icon")){
//              $('.folder-double-click, .folderIcon_list,.double-click').removeClass('add_background');
//              $('.folder-double-click, .folderIcon_list,.double-click').removeClass('add_border');
//            }
//          $('.dragSelectionEL').removeClass('add_border_onlist');

            if(
                $(e.target).closest("#folder_bar").hasClass("fileaction_icon") ||
                $(e.target).closest("#contentdata").hasClass("padResp") ||
                $(e.target).closest(".ui-autocomplete").hasClass('ui-menu') ||
                $(e.target).closest(".bootbox-confirm").hasClass('bootbox')
            ){} else {
                $('.folder-double-click, .folderIcon_list').removeClass('add_background');
                $('.folder-double-click').removeClass('add_border');
            }
            var element = e.target;
            $selection.hide();
            $('#dragFileCount').hide();
            elementFadeIn == false
            click = false;
            $('.add_border').each(function (i) {
                $(this).css('opacity', '1');
            });
            $('.add_border_onlist').each(function (i) {
                $(this).css('opacity', '1');
            });

            //custom might be deleted
            if (ctrl == false) {
                if ($(e.target).closest("#files_bar").hasClass('fileaction_icon')) {
                } else {
                    // for grid
//                    $('#uploaded_files').find('.add_border').removeClass('add_border');
//                    $('[id^=doc_name_]').removeAttr('style');
//                    $('[id^=doc_name_]>h5').removeAttr('style');

                    // for list
//                    $('#listTable > tbody > tr').removeAttr('style');
//                    $('#listTable_wrapper').find('.add_border_onlist').removeClass('add_border_onlist');
                }
            }

            // after drop show file bar again
            if ($('#grid_btn>i').hasClass('icon-grid')) {
                if ($('.add_border_onlist').length == 0) {
                    $('#files_bar').hide();
                    $('.disabled_icon').hide();
                    $('#multiple_file_select_actions').hide();
                } else if ($('.add_border_onlist').length == 1) {
                    $('#files_bar').show();
                    $('.doc_action').show();
                    $('#multiple_file_select_actions').hide();
                } else {
                    $('#files_bar').hide();
                    $('.disabled_icon').hide();
                    $('#multiple_file_select_actions').show();
                }
            }

            if ($('#grid_btn>i').hasClass('icon-list')) {
                if ($('.add_border').length == 0) {
                    $('#files_bar').hide();
                    $('.disabled_icon').hide();
                    $('#multiple_file_select_actions').hide();
                } else if ($('.add_border').length == 1) {
                    $('#files_bar').show();
                    $('.doc_action').show();
                    $('#multiple_file_select_actions').hide();
                } else {
                    $('#files_bar').hide();
                    $('.disabled_icon').hide();
                    $('#multiple_file_select_actions').show();
                }
            }


        } catch (e) {
        }
    })
}

function moveMultipleFileToFolder(folder_id, folder_name, fileCount, filesValuesArr) {
    Metronic.blockUI({
        message: 'Moving...',
        target: $('#created_folder'),
        overlayColor: 'none',
        cenrerY: false,
        boxed: true,
    });
    if (fileCount == 1)
        var message = fileCount + ' file has been moved to "' + folder_name + '" folder successfully.';
    else if (fileCount > 1)
        var message = fileCount + ' files have been moved to "' + folder_name + '" folder successfully.';

    var data_object = {folder_id: folder_id, filesValuesArr: filesValuesArr}
    $.post(base_url + "/document/index/movemultiplefile", data_object, function (data) {
        var page = $('.sidemenu').closest('.open').children().children().eq(1).text();
        if ($.trim(page) == "My Documents") {
            $.each(filesValuesArr, function (i) {
                setTimeout(function (j) {
                    $('#grid_' + filesValuesArr[i]['document_id']).fadeOut(function () {
                        $('#grid_' + filesValuesArr[i]['document_id']).remove();
                        if (data.folderDataList == 0 && $('#created_folder').children().length == 0 && data.currentFolderId != 0) {
                            $('#emptyMsg').html("<strong>Empty!</strong> No documents available for you.");
                            $('#emptyMsg').show();
                        }
                    });
                    $('#list_' + filesValuesArr[i]['document_id']).fadeOut('slow')
                    setTimeout(function () {
                        deleteRow(filesValuesArr[i]['document_id']);
                    }, 500);
                }, 500);
            });
            setTimeout(function () {
                msg = Messenger().post({
                    message: message,
                    hideAfter: 6,
                    hideOnNavigate: true,
                    showCloseButton: true,
                    id: 'saving-msg'
                })
                Metronic.unblockUI($('#created_folder'));
                $("#multiple_file_select_actions").hide();
                $("#files_bar").hide();
            }, 1000);
        }
        if ($('#imp_info').attr('activity_open') == 1) {
            $('.page-sidebar-menu').trigger('click');
            $('#view_detail_btn').trigger('click');
            $('#imp_info').attr('activity_open', 0)
        }
    });
}

function openShareWithMe() {
    $('.sidemenu ').eq(1).trigger('click');
}

$('#share_btn_forMulti').on('click', function (event) {
//    $('#shared_user').html("");
    $.ajax({
        type: "POST",
        url: base_url + "/document/index/sharebody",
        data: {},
        beforeSend: function () {
        },
        success: function (data) {
            $('#share_modal_body').html(data);
            $('#share_submit').hide();
            $('#cancel_share').hide();
            $('#share_done').show();
            $('#save_changes').hide();
            $('#cancelBtn').hide();
            var document_ids = [];
            var document_str = "";
            if ($('#grid_btn>i').hasClass('icon-grid')) {
                $('.add_border_onlist').each(function () {
                    var document_id = $(this).attr("id").split("_")[1];
                    document_str += document_id + "@";
                });
            } else {
                $('.add_border').each(function () {
                    var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
                    document_str += document_id + "@";
                });
            }
            document_str = document_str.substring(0, (document_str.length - 1));
            $('#info_on_share').attr('current_selected_document_ids', document_str);
            get_shared_user_forMulti(document_str);
            listSharedUser_forMulti(document_str);
        }
    });
});

function get_shared_user_forMulti(document_idstr) {
    $("#select2_sample5").select2("val", "");
    var dataa = {document_idstr: document_idstr};
    $.post(base_url + "/document/index/listUser", dataa, function (data) {
        $("#select2_sample5").select2({
            tags: data,
            selectOnBlur: true,
        });
        setTimeout(function () {
            $('#s2id_select2_sample5').on('click', function () {
                $('#share_submit').show();
                $('#cancel_share').show();
                $('#share_done').hide();
            });
            $('#select2_sample5').on('change', function () {
                $(this).valid();
            });
        }, 200);
    });
    Metronic.initSlimScroll('.scroller');
}

function listSharedUser_forMulti(document_idstr) {
    $('.shareNotice').show();
    var dataa = {document_idstr: document_idstr};
    $.post(base_url + "/document/index/listSharedUserMulti", dataa, function (data) {
        var msg = '';
        var download = 0;
        $.each(data.finalArr, function (index, value) {
            download = data.disableFlag;
            var select1 = (value.share_action == 'Comment') ? 'selected="selected"' : '';
            var select2 = (value.share_action == 'View') ? 'selected="selected"' : '';
            var access = (value.access == 'view') ? '<span class="label label-sm label-success label-mini">Can View </span>' : '<span class="label label-sm label-danger label-mini">Can Comment </span>';
            msg += '<div class="row">';
            msg += '<div class="col-md-12 user-info" id="share_user_' + value.share_id + '"><img alt="" src="' + base_url + '/assets/admin/layout/img/avatar.png" class="img-responsive imgWidth"/><div class="details"><div style="color: #5B9BD1;">' + value.email + '</div></div>';
            msg += '<div class="pull-right"><select class="bs-select form-control input-small dropdown-btn shareAction" data-style="btn-success" share_to='+value.shared_to+' data-share_id="' + value.share_id + '" data-sharing_forward="' + value.sharing_forward + '" data-download="' + value.download + '"><option value="Comment" ' + select1 + '>Can comment</option><option value="View" ' + select2 + '>Can view</option></select>';
            msg += '<a href="javascript:;" class="btn btn-xs red shareDelete" share_to='+value.shared_to+' data-share_id="' + value.share_id + '" style="margin-left:20px;margin-top: 5px;"><i class="fa fa-times"></i></a></div></div>';
            msg += '</div>';
        });
        if (msg == "") {
            msg = "<span style='font-style:italic;color:#ABABAE;'>No user accessed.</span>"
            $('#shared_user').html(msg);
            $('#uniform-download').children().eq(0).removeClass('checked');
            $('#uniform-download').children().eq(0).children().eq(0).val(0);
        } else {
            setTimeout(function () {
                $('#shared_user').html("");
                $('#shared_user').append(msg);
                $('#uniform-download').addClass('focus');
                if (download == 0) {
                    $('#uniform-download').children().eq(0).removeClass('checked');
                    $('#uniform-download').children().eq(0).children().eq(0).val(0);
                } else {
                    $('#uniform-download').children().eq(0).addClass('checked');
                    $('#uniform-download').children().eq(0).children().eq(0).val(1);
                }
                ComponentsDropdowns.init();
            }, 1000);
        }
    });
}

function submit_share_mlti(selectClass) {

    $('#shareUser').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
    var doc_data = [];
    var document_str = "";
    $('.' + selectClass).each(function () {
        var doc_object = {};
        var element = $(this);
        if (selectClass == "add_border_onlist")
            var finalElement = element;
        else
            var finalElement = element.parent().eq(0);
        var document_id = finalElement.attr('data-document_id');
        var version_id = finalElement.attr('data-version_id');
        var share_action = $("#share_action").val();
        var sharing_forward = 0;
        var document_title = finalElement.attr('data-document_title');
        var document_type = finalElement.attr('data-document_type');
        var document_content_type = finalElement.attr('document_content_type');
        var version_no = finalElement.attr('data-version');
        var document_path = finalElement.attr('data-document_path');
        var shared_document = 0;
        if ($('.page-bar>table>tbody>tr>td>h4').text().trim() == 'Shared with me') {
            shared_document = 1;
        }
        var doc_object = {
            document_content_type: document_content_type,
            version_no: version_no,
            document_path: document_path,
            shared_document: shared_document,
            document_id: document_id,
            share_action: share_action,
            version_id: version_id,
            sharing_forward: sharing_forward,
            document_title: document_title,
            document_type: document_type
        };
        doc_data.push(doc_object);
        document_str += document_id + "@";
    });
    document_str = document_str.substring(0, (document_str.length - 1));
    var download = $("#download").val();
    var share_email = $("#input1").val();
    var anybody = $("#anybody").val();
    var dataa = {doc_data: doc_data, user_id: user_id, download: download, share_email: share_email,anybody:anybody};
    $.post(base_url + "/document/index/shareMulti", dataa, function (data) {

    }).success(function () {
        //$('#share_modal').modal('hide');
        $('.page-loading').remove();
        $('#share_submit').hide();
        $('#cancel_share').hide();
        $('#share_done').show();
        $('#save_changes').hide();
        $('#cancelBtn').hide();
        listSharedUser_forMulti(document_str);
        get_shared_user_forMulti(document_str);
        $('.removeEmail').each(function () {
            $(this).trigger('click');
        });
    }).fail(function (data) {
        console.log(data);
    });
}

$('#cancelBtn').live("click", function () {
    if ($('#grid_btn>i').hasClass('icon-grid')) {
        var length = $('.add_border_onlist').length;
    } else {
        var length = $('.add_border').length;
    }
    if (length == 1) {
        var document_id = $('#info_on_share').attr('current_selected_document_id');
        var version_id = $('#info_on_share').attr('current_selected_version_id');
        listSharedUser(document_id,version_id);
        get_shared_user(document_id);
    } else {
        var document_str = $('#info_on_share').attr('current_selected_document_ids');
        get_shared_user_forMulti(document_str);
        listSharedUser_forMulti(document_str);
    }
    $('#share_done').show();
    $('#shareUser').show();
    $('#save_changes').hide();
    $('#cancelBtn').hide();
})

// code for star and unstar
$('#favourite_btn_forMulti').on('click', function (event) {
    var title = $('.page_bar>table').find('tr').eq(0).find('td>h4>b').text();
    var action = $(this).attr('data-original-title');
    if (action == 'Add star') {
        var url = base_url + "/document/index/addStarredMulti";
    }
    else {
        var url = base_url + "/document/index/removeStarredMulti";
    }
    var doc_data = [];
    if ($('#grid_btn>i').hasClass('icon-grid')) {
        $('.add_border_onlist').each(function () {
            var document_id = $(this).attr("id").split("_")[1];
            var version_id = $(this).attr('data-version_id');
            var data_starred_id = $(this).attr('data-starred_id');
            var ref_div_id = $(this).attr('id');
            var ref_div_id_second_index = ref_div_id.split("_")[1];
            var doc_object = {document_id: document_id, version_id: version_id, data_starred_id: data_starred_id, ref_div_id: ref_div_id, ref_div_id_second_index: ref_div_id_second_index}
            doc_data.push(doc_object);
        });
    } else {
        $('.add_border').each(function () {
            var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
            var version_id = $(this).parent().eq(0).attr('data-version_id');
            var data_starred_id = $(this).parent().eq(0).attr('data-starred_id');
            var ref_div_id = $(this).parent().eq(0).attr('id');
            var ref_div_id_second_index = ref_div_id.split("_")[1];
            var doc_object = {document_id: document_id, version_id: version_id, data_starred_id: data_starred_id, ref_div_id: ref_div_id, ref_div_id_second_index: ref_div_id_second_index}
            doc_data.push(doc_object);
        });
    }
    var dataa = {doc_data: doc_data, user_id: user_id};
    if ($('#imp_info').attr('favourite_ajax') == 0) {
        $('#imp_info').attr('favourite_ajax', 1)
        $.post(url, dataa, function (data) {
            if (action == 'Add star') {
                $('#favourite_btn_forMulti i').removeClass('icon-star').addClass('glyphicon glyphicon-star');
                $('#favourite_btn_forMulti').attr('data-original-title', 'Remove star');
                $.each(doc_data, function (i) {
                    var document_id = doc_data[i].document_id;
                    var ref_div_id_second_index = doc_data[i].ref_div_id_second_index;
                    $("#doc_name_" + document_id).parent().find('#doc_view').attr('data-starred_id', '1');
                    $('#grid_' + ref_div_id_second_index).attr('is_starred', '1');
                    $('#list_' + ref_div_id_second_index).attr('is_starred', '1');
                })

            } else {
                $('#favourite_btn_forMulti i').removeClass('glyphicon glyphicon-star').addClass('icon-star');
                $('#favourite_btn_forMulti').attr('data-original-title', 'Add star');
                $.each(doc_data, function (i) {
                    var document_id = doc_data[i].document_id;
                    var ref_div_id_second_index = doc_data[i].ref_div_id_second_index;
                    var ref_div_id = doc_data[i].ref_div_id;
                    $("#doc_name_" + document_id).parent().find('#doc_view').attr('data-starred_id', '');
                    $('#' + ref_div_id).attr('is_starred', '0');
                    if ($.trim(title) == "Starred") {
                        $('#grid_' + ref_div_id_second_index).remove();
                        $('#list_' + ref_div_id_second_index).remove();
                        $('.doc_action').hide();
                        blankMsgStarred();
                    }
                })
            }
            setTimeout(function () {
                $('#imp_info').attr('favourite_ajax', 0)
            }, 1000);
        });
    }
});

// Delete item multiple
$('#delete_btn_forMulti').on('click', function () {
    var doc_data = [];
    if ($('#grid_btn>i').hasClass('icon-grid')) {
        $('.add_border_onlist').each(function () {
            var document_id = $(this).attr("id").split("_")[1];
            var version_id = $(this).attr('data-version_id');
            var doc_object = {document_id: document_id, version_id: version_id}
            doc_data.push(doc_object);
        });
    } else {
        $('.add_border').each(function () {
            var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
            var version_id = $(this).parent().eq(0).attr('data-version_id');
            var doc_object = {document_id: document_id, version_id: version_id}
            doc_data.push(doc_object);
        });
    }
    var dataa = {doc_data: doc_data};
    bootbox.confirm("Are you sure you wish to delete these documents.\n When you delete these documents, these will be removed from dashboard of all users that have access to these documents.", function (result) {
        if (result) {
            $.post(base_url + "/document/index/deleteMulti", dataa, function (data) {
                $('.doc_span').hide();
                //for grid update
                $.each(doc_data, function (i) {
                    var document_id = doc_data[i].document_id;
                    $('.doc_div_' + document_id).remove();
                    //for list update
                    //$('#listTable > tbody > tr[data-document_id='+document_id+']').remove();
                    deleteRow(document_id);
                    if ($('#imp_info').attr('activity_open') == 1) {
                        $('.page-sidebar-menu').trigger('click');
                        $('#view_detail_btn').trigger('click');
                        $('#imp_info').attr('activity_open', 0);
                    }
                    $('#multiple_file_select_actions').hide();
                });
                blankMsg();
            });
        }
    });
});


function restoreFiles() {
    var current_type = "file";
    var doc_data = [];
    if ($('#grid_btn>i').hasClass('icon-grid')) {
        $('.add_border_onlist').each(function () {
            var document_id = $(this).attr("id").split("_")[1];
            var version_id = $(this).attr('version_id');
            var folder_id = $(this).attr('folder_id');
            var doc_object = {current_type: current_type, document_id: document_id, version_id: version_id, folder_id: folder_id}
            doc_data.push(doc_object);
        });
    } else {
        $('.add_border').each(function () {
            var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
            var version_id = $(this).parent().eq(0).attr('data-version_id');
            var folder_id = $(this).parent().eq(0).attr('folder_id');
            var doc_object = {current_type: current_type, document_id: document_id, version_id: version_id, folder_id: folder_id}
            doc_data.push(doc_object);
        });
    }


    var dataa = {doc_data: doc_data, current_type: "file"};
    $.post(base_url + "/document/index/restoreMulti", dataa, function (data) {
        //for grid update
        $.each(doc_data, function (i) {
            var document_id = doc_data[i].document_id;
            $('.doc_div_' + document_id).remove();
            //for list update
            $('#listTable > tbody > tr[data-document_id=' + document_id + ']').remove();
        });
        $('.page-sidebar-menu').trigger('click');
        if ($('#imp_info').attr('activity_open') == 1) {
            $('.page-sidebar-menu').trigger('click');
            $('#view_detail_btn').trigger('click');
            $('#imp_info').attr('activity_open', 0)
        }
        blankMsgTrash();
    });
}

function deleteFiles() {
    var current_type = "file";
    var doc_data = [];
    if ($('#grid_btn>i').hasClass('icon-grid')) {
        $('.add_border_onlist').each(function () {
            var document_id = $(this).attr("id").split("_")[1];
            var version_id = $(this).attr('version_id');
            var folder_id = $(this).attr('folder_id');
            var document_path = $(this).attr('data-document_path');
            var doc_object = {current_type: current_type, document_id: document_id, version_id: version_id, folder_id: folder_id, document_path: document_path}
            doc_data.push(doc_object);
            $('.doc_div_' + document_id).children().html('<table style="width: 100%;height: 80%;text-align:center;"><tbody><tr><td><img class="img-responsive" id="doc_view" src="' + base_url + '/img/106.GIF" alt="" style="width:auto;display:inline;"></td></tr></tbody></table>')
            $('.doc_div_' + document_id).children().removeClass('add_border');
            $('#list_' + document_id).children().eq(0).prepend('<span class="delete_loader"><img class="img-responsive" id="doc_view" src="' + base_url + '/img/105.gif" alt="" style="width:auto;display:inline;"></span>')
            $('#list_' + document_id).css('color', '');
            $('#list_' + document_id).css('background-color', "");
        });
    } else {
        $('.add_border').each(function () {
            var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
            var version_id = $(this).parent().eq(0).attr('data-version_id');
            var folder_id = $(this).parent().eq(0).attr('folder_id');
            var document_path = $(this).parent().eq(0).attr('data-document_path');
            var doc_object = {current_type: current_type, document_id: document_id, version_id: version_id, folder_id: folder_id, document_path: document_path}
            doc_data.push(doc_object);
            $('.doc_div_' + document_id).children().html('<table style="width: 100%;height: 80%;text-align:center;"><tbody><tr><td><img class="img-responsive" id="doc_view" src="' + base_url + '/img/106.GIF" alt="" style="width:auto;display:inline;"></td></tr></tbody></table>')
            $('.doc_div_' + document_id).children().removeClass('add_border');
            $('#list_' + document_id).children().eq(0).prepend('<span class="delete_loader"><img class="img-responsive" id="doc_view" src="' + base_url + '/img/105.gif" alt="" style="width:auto;display:inline;"></span>')
            $('#list_' + document_id).css('color', '');
            $('#list_' + document_id).css('background-color', "");
        });
    }
    var dataa = {doc_data: doc_data, current_type: current_type};
    $.post(base_url + "/document/index/deleteForeverMulti", dataa, function (data) {
        $.each(doc_data, function (i) {
            var document_id = doc_data[i].document_id;
            //for grid update
            $('.doc_div_' + document_id).fadeOut('slow');
            //for list update
            $('#listTable > tbody > tr[data-document_id=' + document_id + ']').fadeOut('slow');
            $('.doc_span').hide();

            setTimeout(function () {
                $('.doc_div_' + document_id).remove();
                $('#listTable > tbody > tr[data-document_id=' + document_id + ']').remove();
                var document_length = $('#uploaded_files').children().length;
                blankMsgTrash();
            }, 1000);
        });
        $('.page-sidebar-menu').trigger('click');
        if ($('#imp_info').attr('activity_open') == 1) {
            $('.page-sidebar-menu').trigger('click');
            $('#view_detail_btn').trigger('click');
            $('#imp_info').attr('activity_open', 0)
        }

    }).success(function (data) {
        $('#trashaction').hide();
        getSpaceInfo();
    });
}


$('#expiry_btn_forMulti').on('click', function (event) {
    var form = $("#form_expiry");
    form.validate().resetForm();
    form[0].reset();
    var u = new Date().toString().match(/([-\+][0-9]+)\s/)[1];
    var clientTimeZone = u.substring(0, 3) + ':' + u.substring(3, u.length);
    var time_zone = clientTimeZone;
    var doc_data = [];
    if ($('#grid_btn>i').hasClass('icon-grid')) {
        $('.add_border_onlist').each(function () {
            var document_id = $(this).attr("id").split("_")[1];
            var doc_object = {document_id: document_id}
            doc_data.push(doc_object);
        });
    } else {
        $('.add_border').each(function () {
            var document_id = $(this).parent().eq(0).attr("id").split("_")[1];
            var doc_object = {document_id: document_id}
            doc_data.push(doc_object);
        });
    }
    var dataa = {doc_data: doc_data, time_zone: time_zone};
    $.post(base_url + "/document/index/getDocumentMulti", dataa, function (data) {
        $("#expiry_date").val(data.expiry_date);
        $("#never_expire").val(data.never_expire);
        if (data.never_expire == 1) {
            $('#uniform-never_expire>span').addClass("checked");
            $('.form_datetime').attr('disabled', 'disabled');
            $('#expiry_date').attr('disabled', 'disabled');
            $('.date-set').attr('disabled', 'disabled');
            $('.input-group-btn').addClass("pointer_no");
        } else {
            $('#never_expire').parent().eq(0).removeClass("checked");
            $('.input-group-btn').removeClass("pointer_no");
            $('.form_datetime').removeAttr('disabled', 'disabled');
            $('#expiry_date').removeAttr('disabled', 'disabled');
            $('.date-set').removeAttr('disabled', 'disabled');
        }
    });
});



function expirySubmit_Multi(selectClass) {
    $('#expiryDocument').append('<div class="page-loading" style="display:block !important;z-index:9;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
    var doc_data = [];
    $('.' + selectClass).each(function () {
        var doc_object = {};
        var element = $(this);
        if (selectClass == "add_border_onlist")
            var finalElement = element;
        else
            var finalElement = element.parent().eq(0);
        var document_id = finalElement.attr('data-document_id');
        var version_id = finalElement.attr('data-version_id');
        doc_object = {document_id: document_id, version_id: version_id}
        doc_data.push(doc_object);
    });
    var u = new Date().toString().match(/([-\+][0-9]+)\s/)[1];
    var clientTimeZone = u.substring(0, 3) + ':' + u.substring(3, u.length);
    var never_expire = $("#never_expire").val();
    var expiry_date1 = $("#expiry_date").val().replace("-", "");
    var expiry_date = (expiry_date1 != '') ? expiry_date1 + " " + clientTimeZone : '';
    var dataa = {doc_data: doc_data, never_expire: never_expire, expiry_date: expiry_date};
    $.post(base_url + "/document/index/setExpiryMulti", dataa, function (data) {
    }).success(function () {
        $('.page-loading').remove();
        $('#expiry_modal').modal('hide');
    });
}

function share_done_forMulti(doc_data, download) {
    $('#share_modal_body').html("");
    var dataa = {user_id: user_id, doc_data: doc_data, download: download};
    $.post(base_url + "/document/index/sharedoneMulti", dataa, function (data) {

    }).success(function () {

    });
}

function saveChanges_forMulti(doc_data, share_id, share_action, sharing_forward, download) {
    $('.slimScrollDiv').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
    var dataa = {doc_data: doc_data, share_id: share_id, share_action: share_action, sharing_forward: sharing_forward, download: download};
    $.post(base_url + "/document/index/updateShareMulti", dataa, function (data) {
    }).success(function () {
        $('.page-loading').remove();
        $('#share_done').show();
        $('#shareUser').show();
        $('#save_changes').hide();
        $('#cancelBtn').hide();
    });
}

function deleteShare_forMulti(doc_data, share_id) {
    $('#shareUser').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
    var dataa = {doc_data: doc_data, share_id: share_id};
    $.post(base_url + "/document/index/deleteShareMulti", dataa, function (data) {

    }).success(function () {
        $('#share_user_' + share_id).remove();
        $('.page-loading').remove();
        if ($('#shared_user').children().children().length == 0) {
            $('#shared_user').html('<span style="font-style:italic;color:#ABABAE;">No user accessed.</span>');
        }
    });
}

function getSpaceInfo() {
    $.post(base_url + "/document/index/getSpaceInfo", {}, function (data) {
    }).success(function (data) {
        $('#usedSpace').text(data['usedSpace']);
        $('#document_info').attr("us", data['usedSpaceInByte']);
    });
}

$('#share_btn_forFolder').on('click', function (event) {
//    $('#shared_user').html("");
    $.ajax({
        type: "POST",
        url: base_url + "/document/index/sharebody",
        data: {},
        beforeSend: function () {
        },
        success: function (data) {
            $('#share_modal_body').html(data);
            $('#share_submit').hide();
            $('#cancel_share').hide();
            $('#share_done').show();
            $('#save_changes').hide();
            $('#cancelBtn').hide();
            var folder_id = $('.add_background').closest('.folderIcon').attr("folder_id");
//            var document_title = $('#share_btn').attr('data-document_title');
            $('#info_on_share').attr('current_selected_folder_id', folder_id);
            get_shared_user_OnFolder(folder_id);
            listSharedUser_OnFolder(folder_id);


            $('#rename_document_title').keypress(function (e) {
                if (e.which == 13) {
                    $('#rename_submit').trigger('click');
                }
            });
        }
    });
});

function get_shared_user_OnFolder(folder_id) {
    $("#select2_sample5").select2("val", "");
    var dataa = {folder_id: folder_id};
    $.post(base_url + "/document/index/listUserOnFolder", dataa, function (data) {
        $("#select2_sample5").select2({
            tags: data,
            selectOnBlur: true,
        });
        setTimeout(function () {
            $('#s2id_select2_sample5').on('click', function () {
                $('#share_submit').show();
                $('#cancel_share').show();
                $('#share_done').hide();
            });
            $('#select2_sample5').on('change', function () {
                $(this).valid();
            });
        }, 200);
    });
    Metronic.initSlimScroll('.scroller');
}

function listSharedUser_OnFolder(folder_id) {
    var dataa = {folder_id: folder_id};
    $.post(base_url + "/document/index/listSharedUserOnFolder", dataa, function (data) {
        console.log(data);
        var msg = '';
        //$('#shared_user').html('');
        var download = 0;
        $.each(data, function (index, value) {
            download = value.download;
            var select1 = (value.share_action == 'Comment') ? 'selected="selected"' : '';
            var select2 = (value.share_action == 'View') ? 'selected="selected"' : '';
            //console.log(value);
            var access = (value.access == 'view') ? '<span class="label label-sm label-success label-mini">Can View </span>' : '<span class="label label-sm label-danger label-mini">Can Comment </span>';

            msg += '<div class="row">';
            msg += '<div class="col-md-12 user-info" id="share_user_' + value.share_id + '"><img alt="" src="' + base_url + '/assets/admin/layout/img/avatar.png" class="img-responsive imgWidth"/><div class="details"><div style="color: #5B9BD1;">' + value.email + '</div></div>';
            msg += '<div class="pull-right"><select class="bs-select form-control input-small dropdown-btn shareAction" data-style="btn-success" share_to='+value.shared_to+' data-share_id="' + value.share_id + '" data-sharing_forward="' + value.sharing_forward + '" data-download="' + value.download + '"><option value="Comment" ' + select1 + '>Can comment</option><option value="View" ' + select2 + '>Can view</option></select>';
            msg += '<a href="javascript:;" class="btn btn-xs red shareDelete" share_to='+value.shared_to+' data-share_id="' + value.share_id + '" style="margin-left:20px;margin-top: 5px;"><i class="fa fa-times"></i></a></div></div>';
            msg += '</div>';



            // msg+='<div class="row"><div class="col-md-6 user-info"><img alt="" src="'+base_url+'/assets/admin/layout/img/avatar.png" class="img-responsive imgWidth"/><div class="details"><div><a href="javascript:;">Robert Nilson </a><span class="label label-sm label-success label-mini">Approved </span></div></div></div><div class="col-md-6 user-info"><img alt="" src="'+base_url+'/assets/admin/layout/img/avatar.png" class="img-responsive imgWidth"/><div class="details"><div><a href="javascript:;">Lisa Miller </a> <span class="label label-sm label-danger label-mini">Pending </span></div></div></div></div>';
            //msg+='<p><span>'+value.display_name+'</span>(<span>'+value.email+'</span>)(<span>'+value.access+'</span>)</p>'
        });
        if (msg == "") {
            msg = "<span style='font-style:italic;color:#ABABAE;'>No user accessed.</span>"
            $('#shared_user').html(msg);
            $('#uniform-download').children().eq(0).removeClass('checked');
            $('#uniform-download').children().eq(0).children().eq(0).val(0);
        } else {
            setTimeout(function () {
                $('#shared_user').html("");
                $('#shared_user').append(msg);
                $('#uniform-download').addClass('focus');
                if (download == 0) {
                    $('#uniform-download').children().eq(0).removeClass('checked');
                    $('#uniform-download').children().eq(0).children().eq(0).val(0);
                } else {
                    $('#uniform-download').children().eq(0).addClass('checked');
                    $('#uniform-download').children().eq(0).children().eq(0).val(1);
                }
                ComponentsDropdowns.init();
            }, 1000);
        }
    });
}

function submit_share_forFolder() {
    $('#shareUser').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
    var folder_id = $('.add_background').parent().eq(0).attr("folder_id");
//    var version_id = $('#share_btn').attr('data-version_id');
    var share_email = $("#input1").val();
    var share_action = $("#share_action").val();
    var sharing_forward = $("#sharing_forward").val();
    var download = $("#download").val();
//    var document_title = $('#share_btn').attr('data-document_title');
//    var document_type = $('#share_btn').attr('data-document_type');
//    var document_content_type = $('#share_btn').attr('document_content_type');
//
//    var version_no = $('#share_btn').attr('data-version');
//    var document_path = $('#share_btn').attr('data-document_path');
//    var shared_document = 0;
//    if ($('.page-bar>table>tbody>tr>td>h4').text().trim() == 'Shared with me') {
//        shared_document = 1;
//    }

    var dataa = {folder_id: folder_id,user_id:user_id,share_email:share_email,share_action:share_action,sharing_forward:sharing_forward,download:download};
    $.post(base_url + "/document/index/shareForFolder", dataa, function (data) {

    }).success(function () {
        //$('#share_modal').modal('hide');
        $('.page-loading').remove();
        $('#share_submit').hide();
        $('#cancel_share').hide();
        $('#share_done').show();
        $('#save_changes').hide();
        $('#cancelBtn').hide();
        get_shared_user_OnFolder(folder_id);
        listSharedUser_OnFolder(folder_id);
        $('.removeEmail').each(function () {
            $(this).trigger('click');
        });

        getactivity(document_id, 'D');
    });
}

function deleteShare_forFolder(share_id,share_to){
    var folder_id = $('.add_background').parent().eq(0).attr("folder_id");
    $('#shareUser').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
    var dataa = {folder_id: folder_id, share_id: share_id,share_to:share_to};
    $.post(base_url + "/document/index/deleteShareForFolder", dataa, function (data) {

    }).success(function () {
        $('#share_user_' + share_id).remove();
        $('.page-loading').remove();
        if ($('#shared_user').children().children().length == 0) {
            $('#shared_user').html('<span style="font-style:italic;color:#ABABAE;">No user accessed.</span>');
        }
    });
}

function saveChanges_forFolder(sharing_forward,share_id,share_action,download,share_to) {
    var folder_id = $('.add_background').parent().eq(0).attr("folder_id");
    $('.slimScrollDiv').append('<div class="page-loading" style="display:block !important;"><img src="' + base_url + '/assets/global/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>Loading...</span></div>');
    var dataa = {folder_id: folder_id, share_id: share_id, share_action: share_action, sharing_forward: sharing_forward, download: download,share_to:share_to};
    $.post(base_url + "/document/index/updateShareFolder", dataa, function (data) {
    }).success(function () {
        $('.page-loading').remove();
        $('#share_done').show();
        $('#shareUser').show();
        $('#save_changes').hide();
        $('#cancelBtn').hide();
    });
}
function share_done_forFolder(download) {
    var folder_id = $('.add_background').parent().eq(0).attr("folder_id");
    $('#share_modal_body').html("");
    var dataa = {user_id: user_id, folder_id: folder_id, download: download};
    $.post(base_url + "/document/index/sharedoneFolder", dataa, function (data) {

    }).success(function () {

    });
}


// Some live close modal callback
$('#share_modal').live('hidden.bs.modal', function (e) {
    //$('.page-sidebar-menu').eq(0).trigger('click')
})


// Code will be merged to mobile section by hilal code done by Vipul.
if (InMobile) {
    $('#share_with_me').click(function(){
        $('.icon-user-follow, .icon-note, .icon-trash, .icon-cloud-upload, .icon-clock, .fbarI, #move_bar').addClass('op03');
    });

    $('#myDocuments, #recents, #starreds, #trash, #supports').click(function(){
        $('.icon-user-follow, .icon-note, .icon-trash, .icon-cloud-upload, .icon-clock, .fbarI').removeClass('op03');
    })
}


// will be go to global body click on main.js by hilal on mobile section.
//Start Go to previous directory
function goBack(obj){
    var folder_id= obj.getAttribute('data-folder-id');
    var current_folder_level=obj.getAttribute('data-current-folder-level');
    var user_id = obj.getAttribute('data-user-id');
    var page_name = obj.getAttribute('data-pagename');
    if(current_folder_level == 0){
        $("#page_title_bar").find("h4").removeClass("customForMob");
        obj.setAttribute("data-user-id", 0);
    }
    if(page_name == "sharedwithme" && user_id != 0){
        openFolderOnShare(folder_id, current_folder_level,user_id);
    }else{
        if(page_name != "mydocuments"){
            setNavTitle();
        }
        openFolder(folder_id, current_folder_level);
    }
}

function setNavTitle(){
    $('body').attr('data-pagename','mydocuments');
    $('.page_title b').text('My Documents');
    $('.sidemenu').removeClass('active open');
    $('#myDocuments').addClass('active open');
    $('.trashaction_icon,#empty_trash').hide();
}

function setGoBackDir(data,user_id){
    var current_folder_level = data.length - 1;
    var folder_id = "";
    if(current_folder_level == 0){
        folder_id=0;
    }else{
        folder_id = data[current_folder_level - 1]['folder_id'];
    }
    var goBackBtn=$('.prevDir');
    goBackBtn.attr('data-folder-id',folder_id);
    goBackBtn.attr('data-current-folder-level',current_folder_level);
    goBackBtn.attr('data-user-id',user_id);
}
//End Go to previous directory