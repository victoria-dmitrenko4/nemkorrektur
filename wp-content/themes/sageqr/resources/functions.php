<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__ . '/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__) . '/config/assets.php',
            'theme' => require dirname(__DIR__) . '/config/theme.php',
            'view' => require dirname(__DIR__) . '/config/view.php',
        ]);
    }, true);


/**
 * My AJAX Functions
 */
function js_enqueue_scripts()
{
    wp_enqueue_script("my-ajax-handle", get_stylesheet_directory_uri() . "/assets/scripts/myajax.js", array('jquery'));
    //the_ajax_script will use to print admin-ajaxurl in custom ajax.js
    wp_localize_script('my-ajax-handle', 'the_ajax_script',
        array(
            'myajaxurl' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('myajax-nonce'),
        ));
}

add_action("wp_enqueue_scripts", "js_enqueue_scripts");


add_action('wp_ajax_nopriv_myajax-remove', 'myajax_remove');
add_action('wp_ajax_myajax-remove', 'myajax_remove');
function myajax_remove()
{
    $path = $_POST['path'];
    $fileName = $_POST['fileName'];

    check_ajax_referer('myajax-nonce', 'nonce_code');

    $return_text = 0;
    $abs_path = $path . '/' . $fileName;;

    if (file_exists($abs_path)) {

        is_dir($abs_path) ? rmdir($abs_path) : unlink($abs_path);

        rrmdir($abs_path);

        // Set status
        $return_text = 1;

    } else {

        // Set status
        $return_text = 0;
    }

    // Return status
    echo $return_text;

    exit;
}

// recursively delete folder
function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir . "/" . $object) == "dir") rrmdir($dir . "/" . $object); else unlink($dir . "/" . $object);
            }
        }
        reset($objects);
        rmdir($dir);
    }
}

add_action('wp_ajax_nopriv_rename_item', 'rename_item');
add_action('wp_ajax_rename_item', 'rename_item');

function rename_item()
{
    $path = $_POST['path'];
    $fileName = $_POST['fileName'];
    $newFileName = $_POST['newFileName'];

    $return_text = 0;

    $abs_path_old = $path . '/' . $fileName;

    $abs_path_new = $path . '/' . $newFileName;

    echo $abs_path_new;

    if (file_exists($abs_path_old) && !file_exists($abs_path_new)) {
        rename($abs_path_old, $abs_path_new);
    } else {
        $return_text = 0;
    }

    echo $return_text;

    exit;
}


add_action('wp_ajax_nopriv_myajax-updaterev', 'myajax_updaterev');
add_action('wp_ajax_myajax-updaterev', 'myajax_updaterev');
function myajax_updaterev()
{

    $user_ID = $_POST['user_id'];
    echo $user_ID;
    $usr_upload_dir = $_POST['usr_upload_dir'];
    echo $usr_upload_dir;
    var_dump($_POST['my_file_upload']);

    check_ajax_referer('myajax-nonce', 'nonce_code');

}

add_action('wp_ajax_nopriv_render_folder', 'render_folder');
add_action('wp_ajax_render_folder', 'render_folder');

function scanAllDir($dir, $folder_id)
{

    $result = [];
    foreach (glob($dir . '/*', GLOB_ONLYDIR) as $filename) {
        if ($filename[0] === '.') continue;

        if ($filename == $dir . '/' . $folder_id) {

            $filePath = $filename;

            foreach (scandir($filePath, 1) as $childFilename) {

                if ($childFilename[0] === '.') continue;

                $result[] = $filePath . '/' . $childFilename;
            }
        }
    }

    return $result;
}

function render_folder()
{

    $current_user_id = get_current_user_id();
    $usr_entries = array();
    $usr_files = array();
    $usr_dirs = array();
    $usr_upload_dir = get_theme_root() . '/UserDir/' . $current_user_id;
    $usr_upload = get_theme_root_uri() . '/UserDir/' . $current_user_id;
    $theme_path = get_theme_root();


    if (file_exists($usr_upload_dir)) {

        $folder_id = isset($_POST['folder_id']) ? $_POST['folder_id'] : '';
        $parent_id = $_POST['parent_id'];

        $current_folder = $usr_upload_dir . '/' . $folder_id;

        $usr_entries[] = scanAllDir($usr_upload_dir, $folder_id);

    }

    if (isset($_POST['folder_path'])) {

        $current_folder = $_POST['folder_path'] . '/' . $folder_id;

        if (is_null($parent_id)) {

            $usr_entries[0] = array_map(function ($path) {
                return $_POST['folder_path'] . '/' . $path;
            },
                scandir($_POST['folder_path'], 1));
        } else {

            $usr_entries[0] = scanAllDir($_POST['folder_path'], $folder_id);
        }

        $folder_id = basename($_POST['folder_path']);


    }

    for ($i = 0; $i < count($usr_entries[0]); $i++) {

        if (is_dir($usr_entries[0][$i])) {

            $usr_dirs[] = $usr_entries[0][$i];

        } else {
            $usr_files[] = $usr_entries[0][$i];
        }
    }
    ?>

    <div id="folderBreadCrumb" class="page-bar">
        <ul>
            <li><a id="bread_0" href="javascript:void(0);" folder_id=0 folder_name="My Documents"
                   class="breadCrumb mix_all"
                   onclick="homeFolder();updateFolderStatus(0);" title="Home">Home <i class="fas fa-chevron-right"></i></a>
            </li>
            <?php

            if (intval($parent_id) == 0 && is_numeric($parent_id)) {

                $path = '<li><h6 class="breadCrumb mix_all lastElem" data-path="' . $current_folder . '">' . basename($current_folder) . '</h6></li>';

            } else {

                if (isset($_POST['step1'])) {

                    $path = '<li><a class="breadCrumb mix_all" href="javascript:void(0);" onclick="updateFolderStatus(\'' . $_POST['step1']
                        . '\');" data-path="' .
                        $usr_upload_dir . '/' . $_POST['step1'] . '" data-step="1">' . $_POST['step1'] . ' <i class="fas fa-chevron-right"></i></a></li>';

                }

                if (isset($_POST['step2'])) {

                    $path .= '<li><a class="breadCrumb mix_all" href="javascript:void(0);" onclick="updateFolderStatus(\'' . $_POST['step2']
                        . '\');" data-path="' .
                        $usr_upload_dir . '/' . $_POST['step1'] . '/' . $_POST['step2'] . '" data-step="2">' . $_POST['step2'] . '<i class="fas fa-chevron-right"></i></a></li>';

                }

                if (isset($_POST['step3'])) {

                    $path .= '<li><a class="breadCrumb mix_all" href="javascript:void(0);" onclick="updateFolderStatus(\'' . $_POST['step3']
                        . '\');" data-path="' .
                        $usr_upload_dir . '/' . $_POST['step1'] . '/' . $_POST['step2'] . $_POST['step3'] . '" data-step="3">' . $_POST['step3'] . '<i class="fas fa-chevron-right"></i></a></li>';

                }

                if (isset($_POST['step4'])) {

                    $path .= '<li><a class="breadCrumb mix_all" href="javascript:void(0);" onclick="updateFolderStatus(\'' . $_POST['step4']
                        . '\');" data-path="' .
                        $usr_upload_dir . '/' . $_POST['step1'] . '/' . $_POST['step2'] . $_POST['step3'] . $_POST['step4'] . '" data-step="4">' . $_POST['step4'] . '<i class="fas fa-chevron-right"></i></a></li>';

                }


                $path .= '<li><h6 class="breadCrumb mix_all lastElem" data-path="' . $current_folder . '" >' . basename($current_folder) . '</h6></li>';


            }

            echo $path ?>
        </ul>
    </div>

    <div id="current_dir">

        <div id="created_folder" class="row created_folder">
            <?php


            for ($i = 0; $i < count($usr_dirs); $i++) {
                $dir_name = basename($usr_dirs[$i]);

                if (strpos($dir_name, '.') === false) {

                    ?>

                    <div id="folder-bar<?php echo $i ?>"
                         class="mix-inner folder-bar folder-double-click col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div customId="<?php echo $dir_name ?>" data-parent="<?php echo $folder_id ?>"
                             data-path="<?php echo $current_folder ?>" for="folder"
                             id="folder_info_wrapper<?php echo $i ?>"
                             class="folder_info_wrapper">
                    <span class="folder_icon folder_icon_custom">
                        <i class="fas fa-folder"></i>
                    </span>
                            <span class="folder_name" customId="<?php echo $dir_name ?>" id="folder_name" for="folder">
                        <?php echo $dir_name; ?>
                    </span>
                        </div>
                    </div>
                <?php }
            }
            ?>
        </div>

        <div id="uploaded_files" class="row">
            <?php

            if ($usr_files) {
                for ($i = 0; $i < count($usr_files); $i++) {
                    $file_name = basename($usr_files[$i]);
                    ?>
                    <!-- grid column -->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 card_wrap">
                        <!-- .card -->

                        <div id="card_figure" class="card card-figure">
                            <!-- .card-figure -->
                            <figure class="figure">
                                <!-- .figure-img -->
                                <div class="figure-attachment">

                                    <input type="hidden" name="folder_name" value='<?php echo $file_name; ?>'>
                                    <?php
                                    $no_path = $usr_upload . '/' . $folder_id;

                                    $folder_path = isset($_POST['folder_path']) ? str_replace($theme_path . '/UserDir/' . $current_user_id, $usr_upload, $current_folder) : $no_path;
                                    //

                                    echo '<a href="#" class="fig_img"><img data-usr="' . $current_user_id . '" data-src="' . $file_name . '" src="' . $folder_path . '/' . $file_name . '" class="current" id="img_' . $i . '"></a>';
                                    ?>

                                </div>
                                <!-- /.figure-img -->
                                <figcaption class="figure-caption">
                                    <ul class="list-inline d-flex text-muted mb-0">
                                        <li class="list-inline-item text-truncate mr-auto">
                                            <span><i class="fas fa-file-image mx-1"></i></span><span><?php echo $file_name; ?></span>
                                        </li>
                                    </ul>
                                </figcaption>
                            </figure>
                            <!-- /.card-figure -->

                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /grid column -->
                <?php }
            } else echo '<p class="notice"><strong>Empty!</strong> This folder is empty.</p>'; ?>
        </div>
    </div>

    <?php

    exit();
}

add_action('wp_ajax_nopriv_home_folder', 'home_folder');
add_action('wp_ajax_home_folder', 'home_folder');

function home_folder()
{


    $current_user_id = get_current_user_id();
    $usr_entries = array();
    $usr_files = array();
    $usr_dirs = array();

    $usr_upload_dir = get_theme_root() . '/UserDir/' . $current_user_id;

    if (file_exists($usr_upload_dir)) {

        $usr_entries = scandir($usr_upload_dir, 1);
    }

    for ($i = 0; $i < count($usr_entries) - 2; $i++) {
        $full_path_usr_file = $usr_upload_dir . '/' . $usr_entries[$i];
        if (is_dir($full_path_usr_file)) {
            $usr_dirs[] = $usr_entries[$i];
        } else {
            $usr_files[] = $usr_entries[$i];
        }
    }

    ?>

    <div id="current_dir">
        <div id="created_folder" class="row created_folder">
            <?php for ($i = 0; $i < count($usr_dirs); $i++) { ?>

                <div id="folder-bar<?php echo $i ?>"
                     class="mix-inner folder-bar folder-double-click col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                    <div customId="<?php echo $usr_dirs[$i] ?>" data-parent="" for="folder"
                         id="folder_info_wrapper<?php echo $i ?>"
                         class="folder_info_wrapper">
                    <span class="folder_icon folder_icon_custom">
                        <i class="fas fa-folder"></i>
                    </span>
                        <span class="folder_name" customId="<?php echo $usr_dirs[$i] ?>" id="folder_name" for="folder">
                        <?php echo $usr_dirs[$i]; ?>
                    </span>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div id="uploaded_files" class="row">
            <?php
            if ($usr_files) {
                for ($i = 0; $i < count($usr_files); $i++) {
                    $file_name = basename($usr_files[$i]);
                    ?>
                    <!-- grid column -->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 card_wrap">
                        <!-- .card -->

                        <div id="card_figure" class="card card-figure">
                            <!-- .card-figure -->
                            <figure class="figure">
                                <!-- .figure-img -->
                                <div class="figure-attachment">

                                    <input type="hidden" name="folder_name" value='<?php echo $usr_files[$i]; ?>'>
                                    <?php
                                    echo '<a href="#" class="fig_img"><img data-usr="' . $current_user_id . '" data-src="' . $usr_files[$i] . '" src="' . get_theme_root_uri() . "/UserDir/" . $current_user_id . "/" . $usr_files[$i] . '" class="current" id="img_' . $i . '"></a>';
                                    ?>

                                </div>
                                <!-- /.figure-img -->
                                <figcaption class="figure-caption">
                                    <ul class="list-inline d-flex text-muted mb-0">
                                        <li class="list-inline-item text-truncate mr-auto">
                                            <span><i class="fas fa-file-image mx-1"></i></span><span><?php echo $usr_files[$i]; ?></span>
                                        </li>
                                    </ul>
                                </figcaption>
                            </figure>
                            <!-- /.card-figure -->

                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /grid column -->
                <?php }
            } else echo '<p class="notice"><strong>Empty!</strong> This folder is empty.</p>'; ?>
        </div>
    </div>
    <?php

    exit();
}

// tagsinput
add_action('wp_enqueue_scripts', 'tagsinput_css');
function tagsinput_css()
{
    wp_enqueue_style('tagsinput_css',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css');
    wp_enqueue_style('tagsinput_css_head',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-typeahead.css');
    wp_enqueue_style('styles', get_theme_root_uri() . '/sageqr/dist/styles/style.css');
    wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.8.2/css/all.css');


    // wp_enqueue_script( 'main-js', get_theme_root_uri() . '/sageqr/dist/scripts/main.js', '', .min'1.0.0', true);
    wp_enqueue_script('tagsinput_js',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js', '', '0.8.0',
        true);
}

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 100);

function my_jquery_enqueue()
{
    wp_deregister_script('jquery');
    wp_register_script('jquery', "https://code.jquery.com/jquery-3.4.1.min.js", false, null);
    wp_enqueue_script('jquery');
}


add_action('wp_ajax_nopriv_process_upload', 'process_upload');
add_action('wp_ajax_process_upload', 'process_upload');

function process_upload()
{

    $user_ID = get_current_user_id();
    $usr_upload_dir = $_POST['usr_upload_dir'];

    require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');

    $i = 1;

    $profilepicture = $_FILES['file']; //print_r($profilepicture);
    $new_file_path = $usr_upload_dir . '/' . $profilepicture['name'];
    $new_file_mime = mime_content_type($profilepicture['tmp_name']);

    if (empty($profilepicture))
        die('File is not selected.');

    if ($profilepicture['error'])
        die($profilepicture['error']);

    if ($profilepicture['size'] > wp_max_upload_size())
        die('It is too large than expected.');

    if (!in_array($new_file_mime, get_allowed_mime_types()))
        die('WordPress doesn\'t allow this type of uploads.');

    while (file_exists($new_file_path)) {
        $new_file_path = $usr_upload_dir . '/' . $i . '_' . $profilepicture['name'];
        $i++;
    }
    if (move_uploaded_file($profilepicture['tmp_name'], $new_file_path)) {

        chmod($new_file_path, 0777);

    }

    load_files($user_ID, $usr_upload_dir);


}

function load_files($current_user_id, $usr_upload_dir)
{

    $usr_entries = array();
    $usr_files = array();
    $usr_dirs = array();

    $usr_upload = get_theme_root_uri() . '/UserDir/' . $current_user_id;
    $theme_path = get_theme_root();


    if (file_exists($usr_upload_dir)) {

        $usr_entries = scandir($usr_upload_dir, 1);


    }

    for ($i = 0; $i < count($usr_entries) - 2; $i++) {
        $full_path_usr_file = $usr_upload_dir . '/' . $usr_entries[$i];
        if (is_dir($full_path_usr_file)) {
            $usr_dirs[] = $usr_entries[$i];
        } else {
            $usr_files[] = $usr_entries[$i];
        }
    }

    ?>

    <div id="created_folder" class="row created_folder">
        <?php


        for ($i = 0; $i < count($usr_dirs); $i++) {
            $dir_name = basename($usr_dirs[$i]);

            if (strpos($dir_name, '.') === false) {

                ?>

                <div id="folder-bar<?php echo $i ?>"
                     class="mix-inner folder-bar folder-double-click col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                    <div customId="<?php echo $dir_name ?>" data-parent="<?php echo '' ?>"
                         data-path="<?php echo $usr_upload_dir ?>" for="folder"
                         id="folder_info_wrapper<?php echo $i ?>"
                         class="folder_info_wrapper">
                    <span class="folder_icon folder_icon_custom">
                        <i class="fas fa-folder"></i>
                    </span>
                        <span class="folder_name" customId="<?php echo $dir_name ?>" id="folder_name" for="folder">
                        <?php echo $dir_name; ?>
                    </span>
                    </div>
                </div>
            <?php }
        }
        ?>
    </div>

    <div id="uploaded_files" class="row">
        <?php

        if ($usr_files) {
            for ($i = 0; $i < count($usr_files); $i++) {
                $file_name = basename($usr_files[$i]);
                ?>
                <!-- grid column -->
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 card_wrap">
                    <!-- .card -->

                    <div id="card_figure" class="card card-figure">
                        <!-- .card-figure -->
                        <figure class="figure">
                            <!-- .figure-img -->
                            <div class="figure-attachment">

                                <input type="hidden" name="folder_name" value='<?php echo $file_name; ?>'>
                                <?php

                                echo '<a href="#" class="fig_img"><img data-usr="' . $current_user_id . '" data-src="' . $file_name . '" data-theme="' . $theme_path . '" src="" class="current" id="img_' . $i . '"></a>';
                                ?>

                            </div>
                            <!-- /.figure-img -->
                            <figcaption class="figure-caption">
                                <ul class="list-inline d-flex text-muted mb-0">
                                    <li class="list-inline-item text-truncate mr-auto">
                                        <span><i class="fas fa-file-image mx-1"></i></span><span><?php echo $file_name; ?></span>
                                    </li>
                                </ul>
                            </figcaption>
                        </figure>
                        <!-- /.card-figure -->

                    </div>
                    <!-- /.card -->
                </div>
                <!-- /grid column -->
            <?php }
        } else echo '<p class="notice"><strong>Empty!</strong> This folder is empty.</p>'; ?>
    </div>


    <?php

    exit();
}

add_action('wp_ajax_nopriv_create_new_folder', 'create_new_folder');
add_action('wp_ajax_create_new_folder', 'create_new_folder');
function create_new_folder()
{

    $usr_upload_dir = $_POST['usr_upload_dir'];
    $folder_name = $_POST['folder_name'];
    $user_ID = get_current_user_id();

    $folder = test_input($folder_name);

    if (!is_file($folder) && !is_dir($folder)) {

        wp_mkdir_p($usr_upload_dir . '/' . $folder_name);
        load_files($user_ID, $usr_upload_dir);

    } else {
        echo "{$folder} exists and is a valid folder";
    }

    exit;
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

/* share items */
add_action('wp_ajax_nopriv_share_item', 'share_item');
add_action('wp_ajax_share_item', 'share_item');

function share_item(){

    global $wpdb;

    $table_name = 'wp_userfiles';
    $sql = 'CREATE TABLE '.$table_name.'(
        id INTEGER NOT NULL PRIMARY KEY,
        file_name VARCHAR(255) NOT NULL,
        file_path VARCHAR(255) NOT NULL,
        user_id INTEGER NOT NULL)';

    $table_exist = maybe_create_table($table_name, $sql);

    $emails = $_POST['emails'];

    var_dump($table_exist );
    die;

}
