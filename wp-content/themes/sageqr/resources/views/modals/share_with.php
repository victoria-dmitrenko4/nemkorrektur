<!-- Share With File Modal -->
<div class="modal fade" id="share_modal" tabindex="-1" role="dialog" aria-labelledby="shareWithModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">

    <div class="modal-content">
        <div class="modal-header ui-draggable-handle">
            <!-- 											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> -->
            <h4 class="modal-title">Share With</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
        <div id="share_modal_body" class="modal-body pull-left" style="width:100%;">

            <form action="#" type="POST" id="share_form" class="form-horizontal container" novalidate="novalidate">

                <div class="row" style="padding:0;" id="shareUser">


                    <div class="col-md-8" style="padding-left:0px;margin-bottom:10px;">

                        <input type="text"
                               placeholder="Enter names or email address..."
                               class="list_for_tags ui-autocomplete-input"
                               autocomplete="off"
                               data-role="tagsinput"
                               style="width: 100%;">

                        <input type="hidden" name="select2_sample5" id="input1" class="form-control select2"
                               placeholder="Enter names or email address..." value="" style="display: none;">
                        <input type="hidden" name="invalid_email" id="invalid_email" value="">
                        <span id="invalid_email-error" class="help-block help-block-error" >Some email IDs are not correct.</span>
                    </div>
                    <div class="col-md-4" style="padding:0px;">
                        <select class="bs-select form-control" data-show-subtext="true" id="share_action">

                            <option value="Comment">Can comment</option>
                            <option value="View">Can view</option>
                        </select>
                        <button type="submit" class="btn blue btn-sm pull-right" id="share_submit"
                                style="margin-top: 20px;">Share
                        </button>
                        <button onclick="resetEmailBar();" type="button" id="cancel_share"
                                class="btn default btn-sm pull-right"
                                style="margin-top: 20px; margin-left: 6px; margin-right: 7px;">Cancel
                        </button>
                    </div>


                </div>
            </form>
            <div class="col-md-12" style="padding:0px;">
                <div class="tab-pane" style="border-bottom:1px solid #f1f1f1;padding-bottom:5px;">
                    <h4 class="shared-user-headding clearfix">Who has access</h4>
                    <div class="slimScrollDiv"
                         style="position: relative; overflow: hidden; width: auto; height: 185px;">
                        <div id="shared_user" class="scroller"
                             style="height: 185px; padding-left: 20px; overflow: hidden; width: auto;"
                             data-always-visible="1" data-rail-visible1="1" data-initialized="1"><span
                                    style="font-style:italic;color:#ABABAE;">No user accessed.</span></div>
                        <div class="slimScrollBar"
                             style="background: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 185px;"></div>
                        <div class="slimScrollRail"
                             style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div>
                    </div>
                    <div class="shareNotice">Some of the selected files may be shared with users who are not in this
                        list. To change their sharing options, check the files individually.
                    </div>
                    <span class="share-user-bottom-headding">Restrict</span>
                </div>
            </div>

            <div id="info_on_share" style="display:none;" current_selected_document_id="" current_selected_version_id=""></div>
        </div>
        <div class="modal-footer" style="clear:both;border-style:none;padding-top:0px;">
            <div class="pull-left">
                <label class="pull-left">
                    <div class="pull-left">
                            <span class=""><div class="checker" id="uniform-download"><span>
                                        <input type="checkbox"
                                               name="download"
                                               id="download"
                                               onclick="$(this).attr('value', this.checked ? 1 : 0)"
                                               value="0"></span>
                                </div>
                            </span>
                    </div>
                    Disable download for everyone
                </label>
                <label class="pull-left">
                    <div class="pull-left">
                            <span class=""><div class="checker" id="uniform-anybody"><span><input type="checkbox"
                                                                                                  name="anybody"
                                                                                                  id="anybody"
                                                                                                  onclick="$(this).attr('value', this.checked ? 1 : 0)"
                                                                                                  value="0"></span></div></span>
                    </div>
                    Allow comments without login
                </label>
            </div>
            <button type="button" class="btn blue btn-sm pull-right done" data-dismiss="modal"
                    style="margin-top:10px;" onclick="share_done();" id="share_done">DONE
            </button>
            <button type="button" class="btn blue btn-sm pull-right" style="margin-top: 10px; display: none;"
                    id="save_changes">Save changes
            </button>
            <button type="button" class="btn default btn-sm pull-right"
                    style="margin-top: 10px; margin-right: 10px; display: none;" id="cancelBtn">Cancel
            </button>
        </div>
    </div>

</div>
</div>

