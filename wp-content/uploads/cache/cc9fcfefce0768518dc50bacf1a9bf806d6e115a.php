<!-- ============================================================== -->
<!-- left sidebar -->
<!-- ============================================================== -->
<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">

                    <li class="nav-item">
                        <a class="nav-link add-new" href="" data-toggle="collapse" aria-expanded="false"
                           data-target="#submenu-10" aria-controls="submenu-10"><i class="fas fa-plus"> </i> Add New</a>
                        <div id="submenu-10" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item fileinput-button">
                                </li>

                                <li class="nav-item fileinput-button">
                                    <form id="upload-form" method="post" enctype="multipart/form-data">
                                        <label class="nav-link">
                                            <i class="fas fa-cloud-upload-alt"></i> File<input id="file" type="file"
                                                                                               style="display: none;"
                                                                                               name="profilepicture"/>
                                            <!-- onchange="form.submit()" -->

                                        </label>
                                        <input type="hidden" name="user_id" value='<?php echo $user_ID; ?>'>
                                        <input type="hidden" name="usr_upload_dir"
                                               value='<?php echo $usr_upload_dir; ?>'>
                                    </form>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="new_folder_btn" href="#" data-toggle="modal"
                                       data-target="#exampleModal">
                                        <i class="fas fa-folder-plus"></i> Folder
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link active" href="<?php echo e(home_url('/my-documents/')); ?>"><i class="fas fa-home"> </i>
                            My Documents</a>
                    </li>

                    <li class="nav-item ">
                        <a class="nav-link" href="<?php echo e(home_url('/shared-with-me/')); ?>" data-target="#shared-files">
                            <i class="fas fa-share-alt"></i>
                            Shared with me</a>
                    </li>
                    <li class="nav-item"><a href="#" class="nav-link">
                            <i class="far fa-clock"></i>
                            Recent</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">
                            <i class="far fa-star"></i>
                            Starred
                        </a></li>
                    <li class="nav-item"><a href="#" class="nav-link">
                            <i class="far fa-trash-alt"></i>
                            Trash</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">
                            <i class="fas fa-envelope"></i>
                            Support / Feedback
                        </a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<!-- ============================================================== -->
<!-- end left sidebar -->
<!-- ============================================================== -->


